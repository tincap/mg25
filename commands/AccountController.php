<?php

namespace app\commands;


use app\models\Account;
use app\models\checkpoint\Discover;
use app\models\Email;
use app\models\helpers\ConsoleHelpers;
use app\models\helpers\StringHelpers;
use app\models\Proxy;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Instagram;

class AccountController extends Controller
{
    public function actionChangeUsername($left = null, $right = null)
    {
        if (is_null($left)) {
            $left = 1;
            $right = Account::find()->count();
        } else {
            $right = is_null($right) ? $left : $right;
        }

        for ($id = $left; $id <= $right; $id++) {

            $account = Account::findIdentity($id);
            $instagram = new Instagram();
            $instagram->setProxy($account->getProxyGuzzleFormat());

            $randomUsername = StringHelpers::getRandomUsername();

            try {
                $instagram->login($account->username, $account->password);

                sleep(rand(1, 2));

                $checkUsername = $instagram->account->checkUsername($randomUsername);

                if ($checkUsername->isAvailable()) {
                    ConsoleHelpers::log("Имя не занято", 35);

                    sleep(rand(1, 2));

                    $selfInfo = $instagram->people->getSelfInfo();

                    sleep(rand(1, 2));

                    $instagram->account->editProfile(
                        '',
                        '',
                        $selfInfo->getUser()->getFullName(),
                        $selfInfo->getUser()->getBiography(),
                        $selfInfo->getUser()->getEmail(),
                        $selfInfo->getUser()->getGender(),
                        $randomUsername
                    );

                    ConsoleHelpers::log($id . ". " . $randomUsername, 32);
                }
            } catch (InstagramException $e) {
                ConsoleHelpers::log($id . ". " . $e->getMessage(), 32);
            }
        }
    }

    /**
     * @param null $left
     * @param null $right
     * @throws \Exception
     */
    public function actionLogin($left = null, $right = null)
    {
        if (is_null($left)) {
            $left = 1;
            $right = Account::find()->count();
        } else {
            $right = is_null($right) ? $left : $right;
        }

        for ($id = $left; $id <= $right; $id++) {

            $account = Account::findIdentity($id);

            $instagram = new Instagram();

            $instagram->setProxy($account->getProxyGuzzleFormat());

            try {
                $instagram->login($account->username, $account->password);

                $account->status = Account::STATUS_ACTIVE;
                $account->save();

                ConsoleHelpers::log("$id. OK", 32);

            } catch (InstagramException $e) {
                $discover = new Discover($instagram);

                $account->checkpoint_type = $discover->identityCheckpointTypeByResponse($e->getResponse()->asStdClass());
                $account->status = Account::STATUS_BROKEN;
                $account->save();

                ConsoleHelpers::log("$id. " . $account->checkpoint_type, 31);
            }
        }
    }

    public function actionAdd()
    {
        $file = \Yii::getAlias('@app/data/files/accountList.txt');

        $fp = fopen($file, "r");

        $n = 1;

        while (!feof($fp))
        {
            $str = preg_replace("/\s/", "", fgets($fp, 300));

            $e = explode(':', $str);

            $mail = new Email();
            $mail->username = $e[6];
            $mail->password = $e[7];
            $mail->save();

            $proxy = new Proxy();
            $proxy->ip = $e[2] . ':' . $e[3];
            $proxy->password = $e[4] . ':' . $e[5];
            $proxy->save();

            $account = new Account();
            $account->username = $e[0];
            $account->password = $e[1];
            $account->email_id = $mail->id;
            $account->proxy_id = $proxy->id;
            $account->save();

            echo $n++ . "\n";
        }

        fclose($fp);
    }

    public function actionPending()
    {
        $account = Account::findIdentity(2);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        try {

            $instagram->login($account->username, $account->password);

            $countPendingFriendships = 0;

            $pendingFriendships = $instagram->people->getPendingFriendships();


            echo "list:\n";
            print_r($pendingFriendships->asStdClass());

//                do {
//
//                    $pendingFriendships = $instagram->people->getPendingFriendships();
//
//                    $pendingFriendships->getBigList();
//
//                    $countPendingFriendships += count($pendingFriendships->getUsers());
//
//                } while(count($pendingFriendships->getUsers()) > 0);

        } catch (InstagramException $e) {
            $discover = new Discover($instagram);

            $account->checkpoint_type = $discover->identityCheckpointTypeByResponse($e->getResponse()->asStdClass());
            $account->status = Account::STATUS_BROKEN;
            $account->save();

            ConsoleHelpers::log("{$account->id}. " . $account->checkpoint_type, 31);
        }


    }

    public function actionAccept($accountId)
    {
        ConsoleHelpers::log("Обновленный софт", 32);

        $account = Account::findIdentity($accountId);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        try {
            $instagram->login($account->username, $account->password);

            do {

                $pendingFriendships = $instagram->people->getPendingFriendships();
                ConsoleHelpers::log("Обрабатываем запросы: " . count($pendingFriendships->getUsers()), 35);

                foreach ($pendingFriendships->getUsers() as $user) {
                    $instagram->people->approveFriendship($user->getPk());

                    ConsoleHelpers::log("Приняли запрос пользователя " . $user->getUsername(), 32);
                }
            } while(count($pendingFriendships->getUsers()) > 0);

            ConsoleHelpers::log("Завершили принятие", 35);

        } catch (InstagramException $e) {
            $discover = new Discover($instagram);

            $account->checkpoint_type = $discover->identityCheckpointTypeByResponse($e->getResponse()->asStdClass());
            $account->status = Account::STATUS_BROKEN;
            $account->save();

            ConsoleHelpers::log("$accountId. " . $account->checkpoint_type, 31);
        }
    }
}