<?php

namespace app\commands;


use app\components\Mail\Exceptions\MessageException;
use app\models\Account;
use app\models\checkpoint\Discover;
use app\models\helpers\ConsoleHelpers;
use app\models\checkpoint\CheckpointMail;
use app\models\checkpoint\Exceptions\CheckpointException;
use GuzzleHttp\Exception\GuzzleException;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Instagram;

class CheckpointController extends Controller
{
    public function actionFix($left = null, $right = null)
    {
        if (is_null($left)) {
            $left = 1;
            $right = Account::find()->count();
        } else {
            $right = is_null($right) ? $left : $right;
        }

        for ($id = $left; $id <= $right; $id++) {

            $account = Account::findIdentity($id);

            $instagram = new Instagram();

            $instagram->setProxy($account->getProxyGuzzleFormat());

            if ($account->status == Account::STATUS_ACTIVE) {
                continue;
            }

            if ($account->checkpoint_type == Account::CHECKPOINT_TYPE_SMS_ADD_PHONE || $account->checkpoint_type == Account::CHECKPOINT_TYPE_SMS || $account->checkpoint_type == Account::CHECKPOINT_TYPE_SMS_GO_BACK || $account->checkpoint_type == Account::CHECKPOINT_TYPE_INCORRECT_USERNAME) {
                continue;
            }

            echo "\n\n";
            echo $account->id . ". \n";
            echo "-------------------\n";

            try {
                $account->deleteCookiesFile();

                $instagram->login($account->username, $account->password);

                ConsoleHelpers::log("Аккаунт смог авторизоваться", 32);

                $account->checkpoint_type = null;
                $account->status = Account::STATUS_ACTIVE;

                $account->save();
            } catch (InstagramException $e) {

                sleep(1);

                $discover = new Discover($instagram);

                $account->checkpoint_type = $discover->identityCheckpointTypeByResponse($e->getResponse()->asStdClass());
                $account->status = Account::STATUS_BROKEN;
                $account->save();

                if ($discover->isBanned()) {
                    ConsoleHelpers::log("Аккаунт забанен", 31);
                    continue;
                }

                if ($discover->isSms()) {
                    ConsoleHelpers::log("Аккаунт требует подтверждения смс", 31);
                    continue;
                }

                try {
                    switch ($account->checkpoint_type) {
                        case Account::CHECKPOINT_TYPE_EMAIL:

                                ConsoleHelpers::log("Аккаунт требует подтверждения по Email", 33);

                                $checkpoint = new CheckpointMail($instagram, $discover->getHTML(), $discover->getCheckpointUrl(), $account, true);
                                $checkpoint->doFix();

                                $account->status = Account::STATUS_ACTIVE;
                                $account->checkpoint_type = null;
                                $account->save();

                                ConsoleHelpers::log($account->id . '. Починили', 32);

                            break;

                        default:
                            ConsoleHelpers::log("checkpoint: " . $account->checkpoint_type . ". Софт не умеет чинить эту проблему", 31);
                    }
                } catch (CheckpointException $e) {
                    ConsoleHelpers::log($e->getMessage(), 31);
                } catch (GuzzleException $e) {
                    ConsoleHelpers::log($e->getMessage(), 31);
                } catch (MessageException $e) {
                    ConsoleHelpers::log($e->getMessage(), 31);
                }
            }
        }
    }
}