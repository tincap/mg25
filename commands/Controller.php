<?php

namespace app\commands;


use app\models\helpers\ConsoleHelpers;

class Controller extends \yii\console\Controller
{
    public function beforeAction($action)
    {
        try {
            \Yii::$app->db->createCommand('SET SESSION wait_timeout = 28800')->execute();
            ConsoleHelpers::log('SET SESSION wait_timeout = 28800', 32);
        } catch (\yii\db\Exception $e) {
            ConsoleHelpers::log('Exception MySQL SESSION', 31);
        }

        return parent::beforeAction($action);
    }
}