<?php

namespace app\commands;


use app\models\StableProxy;

class StableProxyController extends Controller
{
    public function actionAdd()
    {
        $file = \Yii::getAlias('@app/data/files/stableProxyList.txt');

        $fp = fopen($file, "r");

        $n = 1;

        while (!feof($fp))
        {
            $str = preg_replace("/\s/", "", fgets($fp, 100));

            $e = explode(':', $str);

            $newStableProxy = new StableProxy();
            $newStableProxy->ip = $e[0] . ':' . $e[1];
            $newStableProxy->password = $e[2] . ':' . $e[3];
            $newStableProxy->save();

            echo $n++ . "\n";
        }

        fclose($fp);
    }
}