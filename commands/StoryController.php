<?php

namespace app\commands;

use app\models\Account;
use app\models\helpers\ConsoleHelpers;
use app\models\helpers\FileHelpers;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Instagram;

class StoryController extends Controller
{
    public function actionStart($photoId = 1, $left = null, $right = null)
    {
        ConsoleHelpers::log("Начинаем с фотографии #$photoId", 34);

        if (is_null($left)) {
            $left = 1;
            $right = Account::find()->count();
        } else {
            $right = is_null($right) ? $left : $right;
        }

        $photoCount = FileHelpers::countFile(\Yii::getAlias('@app/data/story')) - 1;

        for ($id = $left; $id <= $right; $id++) {

            $account = Account::findIdentity($id);

            if ($account->follower_count < 1000) {
                ConsoleHelpers::log($id . ". Меньше 1000 подписчиков", 35);
                continue;
            }

            if ($account->story) {
                ConsoleHelpers::log("{$id}. Уже загружена история", 35);
                continue;
            }

            $instagram = new Instagram(false);

            $instagram->setProxy($account->getProxyGuzzleFormat());

            do {

                $proxyError = false;

                try {
                    $instagram->login($account->username, $account->password);

                    sleep(5);

                    $info = $instagram->people->getSelfInfo();

                    sleep(3);

                    if (!isset($info->user) || !isset($info->user->external_url)) {
                        throw new \Exception("Не смогли вытащить информация с аккаунта");
                    }

                    $instagram->story->uploadPhoto(\Yii::getAlias("@app/data/story/$photoId.jpg"), [
                        'link' => $info->user->external_url
                    ]);

                    ConsoleHelpers::log("Загрузили историю на аккаунт #$id", 32);

                    $proxyError = false;
                    $account->story = 1;
                    $account->save();

                    $photoId++;

                    if ($photoId >= $photoCount) {
                        $photoId = 1;
                    }

                } catch (InstagramException $e) {
                    ConsoleHelpers::log($account->id . ". InstagramException: " . $e->getMessage(), 31);

                    if (preg_match('/CONNECT/i', $e->getMessage())) {
                        $proxyError = true;
                    }

                    if (preg_match('/Challenge/i', $e->getMessage())) {
                        $account->working = 0;
                        $account->save();
                    }

                } catch (\Exception $e) {
                    ConsoleHelpers::log($account->id . ". Exception: " . $e->getMessage(), 31);
                }

                sleep(rand(10, 20));

            } while ($proxyError);
        }
    }
}