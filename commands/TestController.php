<?php

namespace app\commands;

use app\models\Account;
use app\models\helpers\ConsoleHelpers;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Instagram;

/**
 * Class TestController
 * @package app\commands
 */
class TestController extends Controller
{
    public function actionIndex()
    {
        $account = Account::findIdentity(1);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        try {
            $instagram->login($account->username, $account->password);

            sleep(1);

            $selfInfo = $instagram->people->getSelfInfo();

            sleep(1);

            $instagram->account->setNameAndPhone(
                $selfInfo->getUser()->getFullName(),
                ''
            );

            ConsoleHelpers::log("OK", 32);
        } catch (InstagramException $e) {
            $e->getResponse();

            print_r($e->getMessage());
        }
    }

    public function actionALife($left = null, $right = null)
    {
        if (is_null($left)) {
            $left = 1;
            $right = Account::find()->count();
        } else {
            $right = is_null($right) ? $left : $right;
        }

        for ($id = $left; $id <= $right; $id++) {

            $account = Account::findIdentity($id);

            $instagram = new Instagram();

            $instagram->setProxy($account->getProxyGuzzleFormat());

            try {
                $instagram->login($account->username, $account->password);

                sleep(rand(3, 7));

                $response = $instagram->people->getSelfFollowers();

                sleep(rand(3, 7));

                $users = $response->getUsers();

                $randomFollower = $users[array_rand($users)];

                $result = $instagram->timeline->getUserFeed($randomFollower->getPk());

                $items = $result->getItems();

                if (rand(1, 10) == 10) {

                    sleep(rand(3, 7));

                    $randomMedia = $items[array_rand($items)];

                    $instagram->media->like($randomMedia->getId());
                }

                return [
                    'status' => 'ok',
                ];
            } catch (InstagramException $e) {
                return [
                    'status' => 'fail',
                    'message' => 'Не смог авторизоваться. ' . $e->getMessage(),
                ];
            }
        }
    }
}
