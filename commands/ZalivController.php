<?php

namespace app\commands;


use app\models\Account;
use app\models\Email;
use app\models\Proxy;

class ZalivController extends Controller
{
    public function actionAdd()
    {
        $file = \Yii::getAlias('@app/data/files/accountList.txt');

        $fp = fopen($file, "r");

        $n = 1;

        while (!feof($fp))
        {
            $str = preg_replace("/\s/", "", fgets($fp, 300));

            $e = explode(':', $str);

            $mail = new Email();
            $mail->username = $e[6];
            $mail->password = $e[7];
            $mail->save();

            $proxy = new Proxy();
            $proxy->ip = $e[2] . ':' . $e[3];
            $proxy->password = $e[4] . ':' . $e[5];
            $proxy->save();

            $account = new Account();
            $account->username = $e[0];
            $account->password = $e[1];
            $account->author = $e[8];
            $account->email_id = $mail->id;
            $account->proxy_id = $proxy->id;
            $account->save();

            echo $n++ . "\n";
        }

        fclose($fp);
    }
}