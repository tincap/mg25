<?php

namespace app\components\Mail;


use app\components\Mail\Request\Message;
use app\components\tincap_psr7\Bot;
use app\components\tincap_psr7\ModelInterface;
use GuzzleHttp\Cookie\FileCookieJar;

class Mail extends Bot
{
    /** @var Request\Account Collection of Account related functions. */
    public $account;

    /** @var Message */
    public $message;

    /**
     * Mail constructor.
     * @param $model
     *
     */
    public function __construct($model)
    {
        /** @var ModelInterface $model */

        parent::__construct($model);

        // Load all function collections.
        $this->account = new Request\Account($this);
        $this->message = new Message($this);

        $this->setCookiesJar(new FileCookieJar($this->getSessionPath() . '/' . $model->username . '.txt'));

        $this->setUserAgent($model->getUserAgent());

        $this->setProxy($model->proxy->ip, $model->proxy->password);
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return 'https://m.mail.ru';
    }

    /**
     * @return array
     */
    public function getMandatoryHeaders()
    {
        return [
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept-Language' => 'en-US,en;q=0.8',
            'Connection' => 'keep-alive',
        ];
    }

    /**
     * @return string
     */
    public function getSessionPath()
    {
        return \Yii::getAlias('@app/components/Mail/sessions');
    }
}