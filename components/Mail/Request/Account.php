<?php

namespace app\components\Mail\Request;


use app\components\tincap_psr7\RequestCollection;

class Account extends RequestCollection
{
    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function login()
    {
        $e = explode('@', $this->parent->getModel()->username);

        $username = $e[0];
        $domain = $e[1];

        return $this->parent->request( 'POST', 'cgi-bin/auth')
            ->setHost('https://auth.mail.ru')
            ->addParam('rand', 129146199)
            ->addPost('post', '')
            ->addPost('mhost', 'm.mail.ru')
            ->addPost('login_from', '')
            ->addPost('Login', $username)
            ->addPost('Domain', $domain)
            ->addPost('Password', $this->parent->getModel()->password)
            ->getResponse();
    }
}