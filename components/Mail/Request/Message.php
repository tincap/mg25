<?php

namespace app\components\Mail\Request;


use app\components\Mail\Exceptions\MessageException;
use app\components\simple_html_dom;
use app\components\tincap_psr7\RequestCollection;
use app\models\helpers\ParserHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class Message extends RequestCollection
{
    /**
     * Вернуть список сообщений
     *
     * @param int $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMessagesList($page = 1)
    {
        $response = $this->parent->request('get', "messages/inbox/?page=$page")->getResponse();
        $html = $response->getBody()->getContents();

        $dom = new simple_html_dom();
        $dom->load($html);

        $boxes = $dom->find('.messageline');

        $messages = [];

        foreach ($boxes as $box) {
            $from    = $box->find('.messageline__from', 0);
            $subject = $box->find('.messageline__subject', 0);
            $link    = $box->find('.messageline__link', 0);

            $messages[] = [
                'id' => explode('/', $link->href)[2],
                'from' => trim($from->innertext),
                'subject' => trim($subject->innertext),
            ];
        }

        return $messages;
    }

    /**
     * Возвращает список id сообщений
     *
     * @param int $page
     * @param string $url
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMessagesIdList($page = 1, $url = 'messages/inbox')
    {
        $url = "messages/inbox/?page=$page";

        $response = $this->parent->request('get', $url)->getResponse();
        $html = $response->getBody()->getContents();

        $dom = new simple_html_dom();
        $dom->load($html);

        $boxes = $dom->find('.messageline');

        $idList = [];

        foreach ($boxes as $box) {
            $link    = $box->find('.messageline__link', 0);
            $idList[] = explode('/', $link->href)[2];
        }

        return $idList;
    }

    /**
     * Найти и вернуть id сообщения Instagram о верификации
     *
     * @return null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getInstagramVerifyMessageId()
    {
        $messages = $this->getMessagesList();

        foreach ($messages as $message) {
//            if (preg_match('/Verify Your Account|Подтвердите свой аккаунт|Підтвердьте свій обліковий запис|Vérifier votre compte|Zweryfikuj konto/iu',  $message['subject']) && preg_match('/instagram/iu',  $message['from'])) {
//                return $message['id'];
//            }

            if (preg_match('/instagram/iu',  $message['from'])) {
                return $message['id'];
            }
        }

        return null;
    }

    /**
     * Вернуть ссылку на подтерждение почты
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTieLink()
    {
        $id = null;

        $messages = array_reverse($this->getMessagesList());

        foreach ($messages as $message) {
            if (preg_match('/(Confirm your email address for Instagram)|(Подтвердите ваш эл)/iu',  $message['subject']) && preg_match('/instagram/iu',  $message['from'])) {
                $id = $message['id'];
            }
        }

        if (is_null($id)) {
            return null;
        }

        $messageText = urldecode($this->showMessage($id));

        preg_match('/(https:\/\/instagram\.com\/accounts\/confirm_email\/\w+\/\w+\/\?app_redirect=[True|False][^"]*)"/iu', $messageText, $found);

        if (isset($found[1])) {
            return $found[1];
        }

        return null;
    }

    /**
     * Показать сообщение
     *
     * @param $id
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function showMessage($id)
    {
        $response = $this->parent->request('get', "/message/$id")->getResponse();
        $html = $response->getBody()->getContents();

        $dom = new simple_html_dom();
        $dom->load($html);

        return $dom->find('.readmsg__text-container', 9)->innertext;
    }

    /**
     * Возвращает код инстаграмма с последнего сообщения
     * @throws MessageException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getVerifyCode()
    {
        $id = $this->getInstagramVerifyMessageId();

        if (is_null($id)) {
            return null;
        }

        $messageText = $this->showMessage($id);

        preg_match('/Your Instagram security code is (\d+)|Ваш код безопасности Instagram (\d+)|<font size="6">(\d+)<\/font>/iu', $messageText, $found);

        if (!empty($found[1])) {
            return (string) $found[1];
        } else {
            if (!empty($found[2])) {
                return (string) $found[2];
            } else {
                if (isset($found[3])) {
                    return (string) $found[3];
                } else {
                    throw new MessageException("Не смогли найти код в сообщении");
                }
            }
        }
    }

    /**
     * Удалить сообщения переданные в массив
     *
     * @param array $messages
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deleteMessages(array $messages)
    {
        $response = $this->parent->request('get', "/messages/inbox")->getResponse();
        $html = $response->getBody()->getContents();

        $formData = ParserHelper::getFormData($html, 0);

        unset($formData['data']['id']);
        unset($formData['data']['spamabuse']);
        unset($formData['data']['move_folderselect']);

        $formData['data']['remove'] = '';

        $postStr = http_build_query($formData['data'], '', '&');

        foreach ($messages as $message) {
            $postStr .= '&id=' . $message;
        }

        if (isset($formData['action'])) {

            $request = new Request('POST', $this->parent->getHost() . '/' . ltrim($formData['action'], '/'), $this->parent->getMandatoryHeaders(), $postStr);

            $client = new Client();

            $response = $client->send($request, [
                'proxy' => $this->parent->getProxy(),
                'cookies' => $this->parent->getCookieJar(),
            ]);

            if ($response->getStatusCode() == 200) {
                return true;
            }

        } else {
            return true;
        }

        return false;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function clearMessages()
    {
        $this->deleteMessages($this->getMessagesIdList());
    }
}