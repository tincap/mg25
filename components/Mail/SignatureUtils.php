<?php

namespace app\components\Mail;


/**
 * Class SignatureUtils
 * @package frontend\components\Mail
 */
class SignatureUtils
{
    /**
     * @return string
     */
    public static function generateUserAgent()
    {
        /** @var array */
        $userAgent = [
            'Mozilla/5.0 (X11; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0',
            'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.8) Gecko/20100202 Firefox/3.5.8 ( .NET CLR 3.5.30729)',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11',
            'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1558.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; rv:24.0) Gecko/20100101 Firefox/24.0',
            'Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B367 Safari/531.21.10',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0',
            'Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.7.62 Version/11.01',
            'Opera/9.80 (X11; Linux x86_64; rv:21.0) Presto/2.7.62 Version/11.01',
        ];

        return $userAgent[array_rand($userAgent)];
    }
}