<?php

namespace app\components\WebParser;

use frontend\components\tincap_psr7\Request;
use frontend\components\tincap_psr7\Bot;
use frontend\models\StableProxy;

class WebParser extends Bot
{
    /**
     * WebParser constructor.
     * @throws \frontend\models\Exceptions\AbsentStableProxyException
     */
    public function __construct()
    {
        parent::__construct();

        $this->setUserAgent(SignatureUtils::generateUserAgent());

        $stableProxy = StableProxy::getRandomStableProxy();
//        $this->setProxy($stableProxy->ip, $stableProxy->password);
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return '';
    }

    /**
     * @return array
     */
    public function getMandatoryHeaders()
    {
        return [
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept-Language' => 'en-US,en;q=0.8',
            'Connection' => 'keep-alive',
        ];
    }

    public function getClient()
    {
        return $this->_client;
    }

    public function request($method, $uri)
    {
        return new Request($this, $method, $uri);
    }
}