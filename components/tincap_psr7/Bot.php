<?php

namespace app\components\tincap_psr7;


use GuzzleHttp\Client;
use GuzzleHttp\Cookie\FileCookieJar;

/**
 * Class Bot
 * @package frontend\components\Mail
 */
abstract class Bot
{
    /** @var Client */
    protected $_client;

    /** @var string */
    protected $_userAgent;

    /** @var FileCookieJar */
    protected $_cookiesJar = false;

    /** @var string */
    protected $_proxy = false;

    protected $_model;

    /**
     * Bot constructor.
     * @param $model
     */
    public function __construct($model)
    {
        $this->_model = $model;
        $this->_client = new Client();
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->_model;
    }

    /**
     * @return string
     */
    abstract public function getHost();

    /**
     * @return array
     */
    abstract public function getMandatoryHeaders();

    /**
     * @return FileCookieJar
     */
    public function getCookieJar()
    {
        return $this->_cookiesJar;
    }

    /**
     * @param $ip
     * @param $password
     */
    public function setProxy($ip, $password)
    {
        $this->_proxy = $this->formattingProxyToGuzzle($ip, $password);
    }

    /**
     * @return mixed
     */
    public function getProxy()
    {
        return $this->_proxy;
    }

    /**
     * @param $userAgent
     */
    public function setUserAgent($userAgent)
    {
        $this->_userAgent = $userAgent;
    }

    /**
     * @param $cookiesJar
     */
    public function setCookiesJar($cookiesJar)
    {
        $this->_cookiesJar = $cookiesJar;
    }

    /**
     * @return mixed
     */
    public function getUserAgent()
    {
        return $this->_userAgent;
    }

    /**
     * @param $ip
     * @param $password
     * @return string
     */
    public function formattingProxyToGuzzle($ip, $password)
    {
        return "http://{$password}@{$ip}";
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->_client;
    }

    /**
     * @param $method
     * @param $uri
     * @return Request
     */
    public function request($method, $uri)
    {
        return new Request($this, $method, $uri);
    }
}