<?php

namespace app\components\tincap_psr7\Exceptions;


use yii\base\Exception;

/**
 * Class PrepareRequestException
 * @package frontend\components\tincap_psr7\Exceptions
 */
class PrepareRequestException extends Exception
{

}