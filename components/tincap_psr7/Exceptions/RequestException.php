<?php

namespace app\components\tincap_psr7\Exceptions;


use GuzzleHttp\Exception\GuzzleException;
use yii\base\Exception;


/**
 * @method string getMessage()
 * @method null getPrevious()
 * @method mixed getCode()
 * @method string getFile()
 * @method int getLine()
 * @method array getTrace()
 * @method string getTraceAsString()
 */
class RequestException extends Exception implements GuzzleException
{

}