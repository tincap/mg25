<?php

namespace app\components\tincap_psr7;


interface ModelInterface
{
    /**
     * @return string
     */
    public function getUserAgent();
}