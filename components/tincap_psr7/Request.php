<?php

namespace app\components\tincap_psr7;


use GuzzleHttp\Exception\GuzzleException;

class Request
{
    /** @var Bot */
    private $_parent;

    /** @var array */
    private $_headers;

    /** @var array */
    private $_posts;

    /** @var array */
    private $_params;

    /** @var string */
    private $_uri;

    /** @var string */
    private $_method;

    /** @var string */
    private $_host;

    /**
     * Request constructor.
     * @param Bot $parent
     * @param $method
     * @param $uri
     * @param array $posts
     * @param array $params
     */
    public function __construct(Bot $parent, $method, $uri, $posts = [], $params = [])
    {
        $this->_method = $method;
        $this->_uri = trim($uri, '/');
        $this->_host = rtrim($parent->getHost(), '/');
        $this->_parent = $parent;
        $this->_posts = $posts;
        $this->_params = $params;

        $this->prepareRequest();
    }

    /**
     * Подгатавливаем client
     */
    public function prepareRequest() {
        foreach ($this->_parent->getMandatoryHeaders() as $key => $value) {
            $this->addHeader($key, $value);
        }

        $this->addHeader('User-Agent', $this->_parent->getUserAgent());
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addHeader($key, $value) {
        $this->_headers[$key] = $value;
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addPost($key, $value)
    {
        $this->_posts[$key] = $value;
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addParam($key, $value)
    {
        $this->_params[$key] = $value;
        return $this;
    }

    /**
     * @param $host
     * @return Request
     */
    public function setHost($host)
    {
        $this->_host = rtrim($host, '/');
        return $this;
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws GuzzleException
     */
    public function getResponse()
    {
        if ($this->_host == '') {
            $host = '';
        } else {
            $host = $this->_host . '/';
        }

        $url = $host . $this->_uri . '?' . http_build_query($this->_params);

        return $this->_parent->getClient()->send(new \GuzzleHttp\Psr7\Request($this->_method, $url), [
            'headers' => $this->_headers,
            'form_params' => $this->_posts,
            'proxy' => $this->_parent->getProxy(),
            'cookies' => $this->_parent->getCookieJar(),
        ]);
    }
}