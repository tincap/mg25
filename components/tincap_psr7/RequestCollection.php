<?php

namespace app\components\tincap_psr7;


class RequestCollection
{
    /**
     * @var Bot
     */
    public $parent;

    /**
     * Constructor.
     * @param Bot $parent
     */
    public function __construct(Bot $parent)
    {
        $this->parent = $parent;
    }
}