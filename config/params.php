<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Секретный ключ для просмотра аккаунтов
    |--------------------------------------------------------------------------
    */

    'secret_key' => 'chechnya-kruto',

    /*
    |--------------------------------------------------------------------------
    | Кол-во фоток на мосту
    |--------------------------------------------------------------------------
    */
    'media_count' => 3,

    /*
    |--------------------------------------------------------------------------
    | Ширина и высота фотографий, которые грузятся на аккаунт
    |--------------------------------------------------------------------------
    */
    'photo_size' => 640,

    /*
    |--------------------------------------------------------------------------
    | Разделительная черта для файла
    |--------------------------------------------------------------------------
    */
    'dividing_line' => '%border%',

    /*
    |--------------------------------------------------------------------------
    | Паузы
    |--------------------------------------------------------------------------
    */

    'caption_pause' => [2, 5], // Задержка при заполнении фоток описаниями
    'photo_pause' => [2, 5], // Зарержка при загрузке фоток
    'replace_pause' => [2, 5], // Зарержка при загрузке фоток

    /*
    |--------------------------------------------------------------------------
    | Sms
    |--------------------------------------------------------------------------
    */

    // Чей sms-service будет выполнять действия
    'sms_services_main_author' => 'rinat',

    'sms_services_keys' => [
        'sms_activate' => [
            'bulat'  => 'e449017B80704d65d33B2B8d3c3ec30B',
            'sharaf' => 'e449017B80704d65d33B2B8d3c3ec30B',
            'rinat'  => 'e449017B80704d65d33B2B8d3c3ec30B',
            'ilnar'  => 'e449017B80704d65d33B2B8d3c3ec30B',
        ],
        'online_sim' => [
            'bulat'  => 'e95613091f52f2f0b0259e1f5f572356',
            'sharaf' => 'e95613091f52f2f0b0259e1f5f572356',
            'rinat'  => 'e95613091f52f2f0b0259e1f5f572356',
            'ilnar'  => '841407246c9718dfd533ffc5e4b33a7c',
        ],
        'sim_sms' => [
            'bulat'  => '1IMFVCXFk8zJxpRhT9QNEX2K9mI6IU',
            'sharaf' => '1IMFVCXFk8zJxpRhT9QNEX2K9mI6IU',
            'rinat'  => '1IMFVCXFk8zJxpRhT9QNEX2K9mI6IU',
            'ilnar'  => '1IMFVCXFk8zJxpRhT9QNEX2K9mI6IU',
        ],
    ]
];
