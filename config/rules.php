<?php

return [

    'api/checkpoint-sms/get-number/<smsService>/<country>' => 'api/checkpoint-sms/get-number/',
    'api/checkpoint-sms/fix/<accountId>/<phoneId>/<phoneNumber>/<smsService>/<smsNumber>' => 'api/checkpoint-sms/fix/',
    'api/checkpoint/fix/<accountId>' => 'api/checkpoint/fix/',

    'zaliv/list/<author>' => 'zaliv/list',

    'api/zaliv/check-link/<username>' => 'api/zaliv/check-link',
    'api/zaliv/direct-check/<accountId>' => 'api/zaliv/direct-check',

    'api/settings/replace/<accountId>/<newZalivName>' => 'api/settings/replace',
    'api/settings/replace-by-zaliv/<accountId>/<oldZalivName>/<newZalivName>' => 'api/settings/replace-by-zaliv',

    'api/account/follow/<accountId>/<uid>' => 'api/account/follow/',

    'api/account/a-life/<accountId>' => 'api/account/a-life',

    'api/media/photo/<accountId>/<folderId>' => 'api/media/photo',

    'api/media/delete/<accountId>/<mediaId>' => 'api/media/delete/',

    'api/account/update-follow-count/<accountId>/<followCount>' => 'api/account/update-follow-count',

    'api/account/check-friendship-status/<accountId>/<uid>' => 'api/account/check-friendship-status',

    'api/<controller>/<action:([\w-]*)>/<accountId:\d+>' => 'api/<controller>/<action>',
];