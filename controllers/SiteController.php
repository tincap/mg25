<?php

namespace app\controllers;

use app\models\Account;
use yii\web\ForbiddenHttpException;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        $key = \Yii::$app->request->get('key');

//        if ($key == \Yii::$app->params['secret_key']) {

            $accountList = Account::find()->all();

            return $this->render('index', [
                'accountList' => $accountList,
            ]);
//        }

        throw new ForbiddenHttpException();
    }
}
