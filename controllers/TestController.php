<?php

namespace app\controllers;


use app\components\Mail\Mail;
use app\models\Email;

class TestController extends Controller
{
    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function actionIndex()
    {
        $mail = Email::findIdentity(2);

        $email = new Mail($mail);

        $r = $email->message->getMessagesIdList();

        echo "<pre>";
        print_r($r);

        $email->message->deleteMessages($r);
    }
}