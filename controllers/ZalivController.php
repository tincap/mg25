<?php

namespace app\controllers;


use app\models\Account;

class ZalivController extends Controller
{
    public function actionList($author)
    {
        $accountList = Account::find()->where(['author' => $author])->all();

        return $this->render('list', [
            'accountList' => $accountList,
        ]);
    }
}