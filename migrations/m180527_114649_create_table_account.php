<?php

use yii\db\Migration;

/**
 * Class m180527_114649_create_table_account
 */
class m180527_114649_create_table_account extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 ENGINE=InnoDB';
        }

        $this->createTable('{{%account}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(40)->notNull()->unique(),
            'password' => $this->string(40)->notNull(),
            'proxy_id' => $this->integer()->null(),
            'email_id' => $this->integer()->null(),
            'status' => $this->smallInteger(1)->unsigned()->defaultValue(1),
            'caption' => $this->smallInteger(1)->unsigned()->defaultValue(0),
            'photo' => $this->smallInteger(1)->unsigned()->defaultValue(0),
            'biographed' => $this->smallInteger(1)->unsigned()->defaultValue(0),
            'zaliv' => $this->string(20)->null(),
            'follow_status' => $this->smallInteger(1)->unsigned()->defaultValue(0),
            'checkpoint_type' => $this->string(30)->null(),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%account}}');
        return true;
    }
}
