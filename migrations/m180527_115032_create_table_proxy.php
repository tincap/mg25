<?php

use yii\db\Migration;

/**
 * Class m180527_115032_create_table_proxy
 */
class m180527_115032_create_table_proxy extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 ENGINE=InnoDB';
        }

        $this->createTable('{{%proxy}}', [
            'id' => $this->primaryKey(),
            'ip' => $this->string(40)->notNull(),
            'password' => $this->string(40)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%proxy}}');
        return true;
    }
}
