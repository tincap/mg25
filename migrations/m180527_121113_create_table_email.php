<?php

use yii\db\Migration;

/**
 * Class m180527_121113_create_table_email
 */
class m180527_121113_create_table_email extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 ENGINE=InnoDB';
        }

        $this->createTable('{{%email}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(40)->notNull()->unique(),
            'password' => $this->string(40)->notNull(),
            'proxy_id' => $this->integer()->null(),
            'user_agent' => $this->string(255)->null(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%email}}');
        return true;
    }
}
