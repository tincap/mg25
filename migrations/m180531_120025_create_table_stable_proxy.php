<?php

use yii\db\Migration;

/**
 * Class m180531_120025_create_table_stable_proxy
 */
class m180531_120025_create_table_stable_proxy extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 ENGINE=InnoDB';
        }

        $this->createTable('{{%stable_proxy}}', [
            'id' => $this->primaryKey(),
            'ip' => $this->string(40)->notNull(),
            'password' => $this->string(40)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%stable_proxy}}');
        return true;
    }
}
