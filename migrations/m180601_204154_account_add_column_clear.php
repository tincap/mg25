<?php

use yii\db\Migration;

/**
 * Class m180601_204154_account_add_column_clear
 */
class m180601_204154_account_add_column_clear extends Migration
{
    public function up()
    {
        $this->addColumn('{{%account}}', 'clear', $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%account}}', 'clear');
        return true;
    }
}
