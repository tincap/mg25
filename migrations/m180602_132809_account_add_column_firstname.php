<?php

use yii\db\Migration;

/**
 * Class m180602_132809_account_add_column_firstname
 */
class m180602_132809_account_add_column_firstname extends Migration
{
    public function up()
    {
        $this->addColumn('{{%account}}', 'firstname', $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%account}}', 'firstname');
        return true;
    }
}
