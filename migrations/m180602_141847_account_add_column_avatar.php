<?php

use yii\db\Migration;

/**
 * Class m180602_141847_account_add_column_avatar
 */
class m180602_141847_account_add_column_avatar extends Migration
{
    public function up()
    {
        $this->addColumn('{{%account}}', 'avatar', $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%account}}', 'avatar');
        return true;
    }
}
