<?php

use yii\db\Migration;

/**
 * Class m180602_225542_account_add_column_follow_count
 */
class m180602_225542_account_add_column_follow_count extends Migration
{
    public function up()
    {
        $this->addColumn('{{%account}}', 'follow_count', $this->smallInteger()->unsigned()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%account}}', 'follow_count');
        return true;
    }
}
