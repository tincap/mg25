<?php

use yii\db\Migration;

/**
 * Class m180605_140803_account_add_info_columns
 */
class m180605_140803_account_add_info_columns extends Migration
{
    public function up()
    {
        $this->addColumn('{{%account}}', 'media_count', $this->smallInteger()->unsigned()->defaultValue(0));
        $this->addColumn('{{%account}}', 'follower_count', $this->smallInteger()->unsigned()->defaultValue(0));
        $this->addColumn('{{%account}}', 'following_count', $this->smallInteger()->unsigned()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%account}}', 'media_count');
        $this->dropColumn('{{%account}}', 'follower_count');
        $this->dropColumn('{{%account}}', 'following_count');
        return true;
    }
}
