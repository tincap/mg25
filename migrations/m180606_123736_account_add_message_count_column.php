<?php

use yii\db\Migration;

/**
 * Class m180606_123736_account_add_message_count_column
 */
class m180606_123736_account_add_message_count_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%account}}', 'message_count', $this->smallInteger()->unsigned()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%account}}', 'message_count');
        return true;
    }
}
