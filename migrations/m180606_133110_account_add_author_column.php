<?php

use yii\db\Migration;

/**
 * Class m180606_133110_account_add_author_column
 */
class m180606_133110_account_add_author_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%account}}', 'author', $this->string(20)->null());
    }

    public function down()
    {
        $this->dropColumn('{{%account}}', 'author');
        return true;
    }
}
