<?php

namespace app\models;


use InstagramAPI\Instagram;
use yii\base\Exception;

class Account extends BasicAccount
{
    const STATUS_BROKEN = 0;
    const STATUS_ACTIVE = 1;

    const FOLLOW_STATUS_NO_ACTIVE = 0;
    const FOLLOW_STATUS_ACTIVE = 1;
    const FOLLOW_STATUS_FEEDBACK = 2;
    const FOLLOW_STATUS_LOGIC = 3;

    const CHECKPOINT_TYPE_SMS = 'sms';
    const CHECKPOINT_TYPE_SMS_ADD_PHONE = 'sms_add_phone';
    const CHECKPOINT_TYPE_SMS_GO_BACK = 'sms_go_back';
    const CHECKPOINT_TYPE_EMAIL = 'email';
    const CHECKPOINT_TYPE_PASSWORD = 'password';
    const CHECKPOINT_TYPE_INCORRECT_USERNAME = 'incorrect_username';
    const CHECKPOINT_TYPE_DISABLED = 'disabled';
    const CHECKPOINT_TYPE_INCORRECT_PASSWORD = 'incorrect_password';
    const CHECKPOINT_TYPE_CHANGED_EMAIL = 'changed_email';
    const CHECKPOINT_TYPE_BLOCKED_EMAIL = 'blocked_email';
    const CHECKPOINT_TYPE_CAPTCHA = 'captcha';
    const CHECKPOINT_TYPE_LOGIN_REQUIRED = 'login_required';
    const CHECKPOINT_TYPE_UNKNOWN = 'unknown';

    /**
     * @param $inbox \InstagramAPI\Response\DirectInboxResponse
     * @param Instagram $instagram
     * @return array
     */
    public function getUnreadThreadList(\InstagramAPI\Response\DirectInboxResponse $inbox, Instagram $instagram)
    {
        $messageList = [];

        foreach ($inbox->getInbox()->getThreads() as $thread) {

            if ($thread->getItems()[0]->getUserId() != $instagram->account_id) {
                $messageList[] = [
                    'thread_id' => $thread->getThreadId(),
                    'user_id' => $thread->getUsers()[0]->getPk(),
                    'username' => $thread->getUsers()[0]->getUsername(),
                    'profile_pic_url' => $thread->getUsers()[0]->getProfilePicUrl(),
                    'timestamp' => $thread->getItems()[0]->getTimestamp() / 1000000,
                    'text' => $thread->getItems()[0]->getItemType() == 'text' ? $thread->getItems()[0]->getText() : ($thread->getItems()[0]->getItemType() == 'like' ? '❤' : ($thread->getItems()[0]->getItemType() == 'link' ? $thread->getItems()[0]->getLink()->getText() : '*Медиа*')),
                ];
            }
        }

        return $messageList;
    }

    public function deleteCookiesFile()
    {
        unlink($this->getCookiesFile());
    }

    public function getCookiesFile()
    {
        return \Yii::getAlias('@vendor/mgp25/instagram-php/sessions/' . $this->username . '/' . $this->username . '-cookies.dat');
    }

    public function translateErrorType()
    {
        switch ($this->checkpoint_type) {
            case self::CHECKPOINT_TYPE_SMS:
                return 'смс';
                break;

            case self::CHECKPOINT_TYPE_SMS_ADD_PHONE:
                return 'смс';
                break;

            case self::CHECKPOINT_TYPE_SMS_GO_BACK:
                return 'смс';
                break;

            case self::CHECKPOINT_TYPE_INCORRECT_PASSWORD:
                return 'Неверный пароль';
                break;

            case self::CHECKPOINT_TYPE_EMAIL:
                return 'email';
                break;

            case self::CHECKPOINT_TYPE_DISABLED:
                return 'Забанен';
                break;

            case self::CHECKPOINT_TYPE_INCORRECT_USERNAME:
                return 'Неверный логин';
                break;

            case self::CHECKPOINT_TYPE_PASSWORD:
                return 'Слетел пароль';
                break;

            case self::CHECKPOINT_TYPE_CHANGED_EMAIL:
                return 'Email угнан';
                break;

            case self::CHECKPOINT_TYPE_CAPTCHA:
                return 'Капча';
                break;

            case self::CHECKPOINT_TYPE_BLOCKED_EMAIL:
                return 'Нерабочий email';
                break;

            default:
                return $this->checkpoint_type == null ? 'Неизвестная ошибка' : $this->checkpoint_type;
        }
    }

    /**
     * Возвращает файл со списком подписок аккаунта
     *
     * @return bool|string
     */
    public function getFollowListFile()
    {
        return \Yii::getAlias('@app/data/follow_list/' . $this->username . '.txt');
    }

    /**
     * @return bool
     */
    public function isSms()
    {
        if ($this->checkpoint_type == Account::CHECKPOINT_TYPE_SMS || $this->checkpoint_type == Account::CHECKPOINT_TYPE_SMS_GO_BACK || $this->checkpoint_type == Account::CHECKPOINT_TYPE_SMS_ADD_PHONE) {
            return true;
        }

        return false;
    }

    public function isBanned()
    {
        if ($this->checkpoint_type == Account::CHECKPOINT_TYPE_INCORRECT_USERNAME || $this->checkpoint_type == Account::CHECKPOINT_TYPE_DISABLED) {
            return true;
        }

        return false;
    }
}