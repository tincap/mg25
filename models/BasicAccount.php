<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "account".
 *
 * @property integer $id
 * @property string  $username
 * @property string  $password
 * @property integer $proxy_id
 * @property integer $email_id
 * @property integer $status
 *
 * @property integer $caption
 * @property integer $photo
 * @property integer $biographed
 * @property integer $clear
 * @property integer $firstname
 * @property integer $avatar
 * @property integer $zaliv
 * @property integer $follow_status
 * @property string  $checkpoint_type
 * @property integer $follow_count
 * @property integer $media_count
 * @property integer $follower_count
 * @property integer $following_count
 * @property integer $message_count
 * @property string  $author
 *
 * @property Proxy $proxy
 * @property Email $email
 */
class BasicAccount extends ActiveRecord
{
    /**
     * Proxy in Guzzle format
     *
     * @return string
     */
    public function getProxyGuzzleFormat()
    {
        return "http://{$this->proxy->password}@{$this->proxy->ip}";
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProxy()
    {
        return static::hasOne(Proxy::class, [
            'id' => 'proxy_id',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmail()
    {
        return static::hasOne(Email::class, [
            'id' => 'email_id',
        ]);
    }

    /**
     * Поиск по username
     *
     * @param $username
     * @return static
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Поиск по id
     *
     * @param $id
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /** @inheritdoc */
    public static function tableName()
    {
        return '{{%account}}';
    }
}