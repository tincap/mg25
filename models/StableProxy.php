<?php

namespace app\models;


use yii\db\ActiveRecord;

/**
 * This is the model class for table "proxy".
 *
 * @property integer $id
 * @property string  $ip
 * @property string  $password
 */
class StableProxy extends ActiveRecord
{
    /**
     * Поиск по id
     *
     * @param $id
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /** @inheritdoc */
    public static function tableName()
    {
        return '{{%stable_proxy}}';
    }
}