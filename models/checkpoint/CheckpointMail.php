<?php

namespace app\models\checkpoint;

use app\models\Account;
use app\models\helpers\ConsoleHelpers;
use app\models\helpers\ParserHelper;
use app\models\checkpoint\Exceptions\CheckpointException;
use app\components\Mail\Mail;
use InstagramAPI\Instagram;

class CheckpointMail
{
    /** @var Instagram $instagram */
    public $instagram;

    /** @var bool */
    public $debug;

    public $html;

    public $checkpointUrl;

    /** @var Account */
    public $account;

    public $token;

    /**
     * CheckpointMail constructor.
     * @param Instagram $instagram
     * @param $html
     * @param $checkpointUrl
     * @param $account
     * @param bool $debug
     * @internal param $account
     * @throws CheckpointException
     */
    public function __construct(Instagram $instagram, $html, $checkpointUrl, $account, $debug = false)
    {
        $this->checkpointUrl = $checkpointUrl;
        $this->instagram = $instagram;
        $this->html = $html;
        $this->account = $account;
        $this->debug = $debug;

        $jsonData = json_decode($instagram->client->getCookieJarAsJSON());

        foreach ($jsonData as $item) {
            if ($item->Name == 'csrftoken') {
                $this->token = $item->Value;
            }
        }

        if (empty($this->token)) {
            throw new CheckpointException("Нет csrftoken в cookies");
        }

        ConsoleHelpers::log("TOKEN: " . $this->token, 35);
    }

    /**
     * @return bool
     * @throws CheckpointException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \app\components\Mail\Exceptions\MessageException
     */
    public function doFix()
    {
        if (preg_match('/This Was Me/iu', $this->html)) {
            if ($this->debug) {
                ConsoleHelpers::log("Instagram спрашивает, ты ли это", 35);
            }

            $formData['data']['choice'] = '0';

            $request = new \GuzzleHttp\Psr7\Request('post', 'https://i.instagram.com/challenge/"');

            $response = $this->instagram->client->api($request, [
                'headers' => [
                    'x-csrftoken' => $this->token,
                ],
                'form_params' => $formData['data'],
            ]);

            $this->html = $response->getBody()->getContents();

            if (preg_match('/This page could not be loaded/i', $this->html)) {
                throw new CheckpointException("This page could not be loaded");
            }
        }

        $form = ParserHelper::getFormData($this->html);

        if ($form == null) {
            if (preg_match('/"back":\s*"([^"]*)"/', $this->html, $found)) {

                if ($this->debug) {
                    ConsoleHelpers::log("Нажимаем go_back", 37);
                }

                $form = [];

                if (isset($found[1])) {
                    $form['action'] = $found[1];

                    $request = new \GuzzleHttp\Psr7\Request('POST', 'https://i.instagram.com' . $form['action']);

                    $response = $this->instagram->client->api($request, [
                        'headers' => [
                            'referer' => 'https://i.instagram.com' . $form['action'],
                            'x-csrftoken' => $this->token,
                        ]
                    ]);

                    $this->html = $response->getBody()->getContents();

                    if (preg_match('/send you a security code to verify your account/iu', $this->html)) {
                        $form = ParserHelper::getFormData($this->html);
                        $form['data']['choice'] = 1;
                    }

                    print_r($form);
                }
            }
        }

        if ($form == null) {
            if (preg_match('/"forward":\s*"([^"]*)"/', $this->html, $found)) {

                $form = [];

                if (isset($found[1])) {
                    if ($this->debug) {
                        ConsoleHelpers::log("forward: $found[1]", 37);
                    }

                    $form['action'] = $found[1];
                    $form['data']['choice'] = 1;
                }
            } else {
                throw new CheckpointException("Не смогли найти forward");
            }
        }

        /*
        |--------------------------------------------------------------------------
        | Парсим страницу
        |--------------------------------------------------------------------------
        */

        if (isset($form['data']['choice']) && $form['data']['choice'] == 0) {
            $this->account->checkpoint_type = Account::CHECKPOINT_TYPE_DISABLED;
            $this->account->status = Account::STATUS_BROKEN;
            $this->account->save();

            throw new CheckpointException("Аккаунт требует подтверждения с помощью SMS. Его невозможно восстановить.");
        }

        $email = $this->account->email;
        $mail = new Mail($email);
        $mail->account->login();
        $mail->message->clearMessages();

        $request = new \GuzzleHttp\Psr7\Request('POST', 'https://i.instagram.com' . $form['action']);

        $response = $this->instagram->client->api($request, [
            'headers' => [
                'referer' => $this->checkpointUrl,
                'x-csrftoken' => $this->token,
            ],
            'form_params' => $form['data'],
        ]);

        $this->html = $response->getBody()->getContents();

        if (preg_match('/This page could not be loaded/i', $this->html)) {
            throw new CheckpointException("This page could not be loaded");
        }

        /*
        |--------------------------------------------------------------------------
        | Нажимаем на кнопку "Подтвердить по эл.почте" и получаем код
        |--------------------------------------------------------------------------
        */

        if ($form instanceof \stdClass) {
            throw new CheckpointException($form->error);
        }

        if (!preg_match('/Enter Security Code/', $this->html)) {

            if ($this->debug) {
                ConsoleHelpers::log("Что-то пошло не так", 31);
                echo $this->html;
            }

            throw new CheckpointException("Не вышла нужная страница для ожидания кода");
        } else {
            if ($this->debug) {
                ConsoleHelpers::log('Enter Security Code', 35);
            }
        }

        if ($this->debug) {
            ConsoleHelpers::log('Mail авторизован', 37);
            ConsoleHelpers::log('Ждем код', 37);
        }

        $attempt = 0;

        do {
            sleep(10);

            $attempt++;

            $code = $mail->message->getVerifyCode();

            if ($this->debug)
                echo ".";

        } while (is_null($code) && $attempt < 15);

        if ($this->debug)
            echo "\n";

        if (is_null($code)) {
            throw new CheckpointException("Код подтверждения не пришел на почту");
        }

        if ($this->debug) {
            ConsoleHelpers::log("Код: $code", 32);
        }

        if (!isset($form['data']['security_code'])) {
            $form = ParserHelper::getFormData($this->html, 0, [
                'security_code' => $code,
            ]);
        }

        if (!is_array($form)) {
            throw new CheckpointException("Не смогли починить аккаунт. formData не вернул массив");
        }

        $request = new \GuzzleHttp\Psr7\Request('POST', 'https://i.instagram.com' . $form['action']);

        $response = $this->instagram->client->api($request, [
            'headers' => [
                'referer' => $this->checkpointUrl,
                'x-csrftoken' => $this->token,
            ],
            'form_params' => $form['data'],
        ]);

        $this->html = $response->getBody()->getContents();

        if (preg_match('/This page could not be loaded/i', $this->html)) {
            throw new CheckpointException("This page could not be loaded");
        }

        if (preg_match('/>Was This You/iu', $this->html)) {
            if ($this->debug) {
                ConsoleHelpers::log("Instagram спрашивает, ты ли это", 37);
            }

            $formData = ParserHelper::getFormData($this->html);

            $formData['data']['choice'] = '0';

            $request = new \GuzzleHttp\Psr7\Request('post', 'https://i.instagram.com' . $formData['action']);

            $response = $this->instagram->client->api($request, [
                'headers' => [
                    'x-csrftoken' => $this->token,
                ],
                'form_params' => $formData['data'],
            ]);

            $this->html = $response->getBody()->getContents();
        }

        if (preg_match('/this code has expired|Please check the code we sent you and try again/i', $this->html)) {
            $mail->message->clearMessages();
            throw new CheckpointException("Код устарел");
        }

        if (preg_match('/(Аккаунт подтвержден)|(Your account has been verified)|(checkpoint\/dismiss)/iu', $this->html)) {

            $this->instagram->client->saveCookieJar();

            return true;
        }

        throw new CheckpointException("После всех действий аккаунт не подтвержден");
    }
}