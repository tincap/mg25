<?php

namespace app\models\checkpoint;


use app\components\SmsServices\BasicSmsServiceModel;
use app\components\SmsServices\Exceptions\SmsServiceException;
use app\models\Account;
use app\models\checkpoint\Exceptions\CheckpointException;
use app\models\helpers\ConsoleHelpers;
use app\models\helpers\ParserHelper;
use InstagramAPI\Instagram;

class CheckpointSms
{
    CONST HOST_I = 'i';
    CONST HOST_WWW = 'www';

    private $_host = self::HOST_I;

    private $_checkpointUrl;

    private $_html;

    private $_checkpointType;

    private $_smsServiceModel;

    private $_debug;

    private $_phone;

    private $_smsNumber;

    private $_instagram;

    private $_token;

    /**
     * CheckpointSms constructor.
     * @param Instagram $instagram
     * @param $html
     * @param $checkpointUrl
     * @param $checkpointType
     * @param bool $debug
     * @param $phone
     * @param int $smsNumber
     * @param null $smsService
     * @throws CheckpointException
     */
    public function __construct(Instagram $instagram, $html, $checkpointUrl, $checkpointType, $debug = false, $phone = null, $smsNumber = 1, $smsService = null)
    {
        $this->_html = $html;
        $this->_smsNumber = $smsNumber;
        $this->_checkpointType = $checkpointType;
        $this->_checkpointUrl = $checkpointUrl;
        $this->_debug = $debug;
        $this->_phone = $phone;

        $this->_instagram = $instagram;

        $this->_smsServiceModel = new BasicSmsServiceModel($debug, $phone, $smsNumber, $smsService);

        $jsonData = json_decode($instagram->client->getCookieJarAsJSON());

        foreach ($jsonData as $item) {
            if ($item->Name == 'csrftoken') {
                $this->_token = $item->Value;
            }
        }

        if (empty($this->_token)) {
            throw new CheckpointException("Нет csrftoken в cookies");
        }
    }

    /**
     * @param string $host
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws SmsServiceException
     * @throws CheckpointException
     */
    public function doFix($host = self::HOST_WWW)
    {
        $this->_host = $host;

        if ($this->_host == self::HOST_WWW) {
            $this->_checkpointUrl = str_replace('i.instagram', 'www.instagram', $this->_checkpointUrl);
        }

        if ($this->_phone == null) {
            if (false === $phone = $this->_smsServiceModel->getNumber()) {
                throw new SmsServiceException("Не смогли получить номер в " . $this->_smsServiceModel->getSmsService());
            }
        }

        $this->checkpointFirstStep();

        $form = $this->checkpointSecondStep();

        $this->checkpointThirdStep($form);
    }

    /**
     * @throws CheckpointException
     * @throws SmsServiceException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function checkpointFirstStep()
    {
        if ($this->_checkpointType == Account::CHECKPOINT_TYPE_SMS || $this->_checkpointType == Account::CHECKPOINT_TYPE_SMS_GO_BACK) {
            $formData = ParserHelper::getFormData($this->_html, 0, [
                'phone_number' => $this->_smsServiceModel->phone['number'],
            ]);
        } elseif ($this->_checkpointType == Account::CHECKPOINT_TYPE_SMS_ADD_PHONE) {

            preg_match('/"csrf_token":\s*"([A-Za-z0-9^"]*)"/iu', $this->_html, $found);

            if (isset($found[1])) {
                $csrftoken = $found[1];
            } else {
                throw new CheckpointException("Не смогли найти csrftoken");
            }

            preg_match('/"forward":\s*"(\/challenge\/[0-9]*[\/]*[A-Za-z0-9]*[\/]*)"/iu', $this->_html, $found);

            if (isset($found[1])) {
                $action = $found[1];
            } else {
                throw new CheckpointException("Не смогли найти forward");
            }

            $formData = [
                'action' => $action,
                'data' => [
                    'phone_number' => $this->_smsServiceModel->phone['number'],
                    'csrfmiddlewaretoken' => $csrftoken,
                ],
            ];

        } else {
            throw new CheckpointException("Неизвестный параметр checkpointType");
        }

        if ($formData == null) {
            throw new SmsServiceException("Пустая форма");
        }

        if (isset($formData['data']) && isset($formData['data']['security_code'])) {

            if ($this->_debug)
                ConsoleHelpers::log('Нажимаем кнопку "Go back"', 36);

            $formData = ParserHelper::getFormData($this->_html, 1);

            $request = new \GuzzleHttp\Psr7\Request('POST', "https://{$this->_host}.instagram.com" . $formData['action']);

            $response = $this->_instagram->client->api($request, [
                'headers' => [
                    'referer' => $this->_checkpointUrl,
                    'x-csrftoken' => $this->_token,
                ],
                'form_params' => $formData['data'],
            ]);

            $html = $response->getBody()->getContents();

            if (preg_match('/This page could not be loaded/i', $html)) {
                throw new CheckpointException("This page could not be loaded");
            }

            $this->_checkpointUrl = $formData['action'];

            if (preg_match('/Enter your phone number/iu', $html)) {

                if ($this->_debug)
                    ConsoleHelpers::log('Успешно нажали "Go back" и теперь аккаунт требует ввести номер телефона', 36);

                $this->_html = $html;

                $formData = ParserHelper::getFormData($html);

                $formData['data']['phone_number'] = $this->_smsServiceModel->phone['number'];
            } else {
                throw new CheckpointException('После нажатия "Go back" произошла ошибка');
            }
        }

        $request = new \GuzzleHttp\Psr7\Request('POST', "https://{$this->_host}.instagram.com" . $formData['action']);

        $response = $this->_instagram->client->api($request, [
            'headers' => [
                'referer' => $this->_checkpointUrl,
                'x-csrftoken' => $this->_token,
            ],
            'form_params' => $formData['data'],
        ]);

        $this->_instagram->client->saveCookieJar();

        $this->_html = $response->getBody()->getContents();

        if (preg_match('/This page could not be loaded/i', $this->_html)) {
            throw new CheckpointException("This page could not be loaded");
        }

        if (preg_match('/please choose a different phone number/iu', $this->_html)) {


            $this->_smsServiceModel->cancel();

            throw new SmsServiceException("Instagram просит другой телефон");
        }

        if (!preg_match('/Enter Security Code/iu', $this->_html)) {
            $this->_smsServiceModel->cancel();
            throw new CheckpointException("Не смогли передать Instagram номер телефона");
        }
    }

    /**
     * @return array|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws SmsServiceException
     */
    public function checkpointSecondStep()
    {
        $code = $this->_smsServiceModel->getCode();

        $formData = ParserHelper::getFormData($this->_html, 0, [
            'security_code' => $code,
        ]);

        $formData['action'] = "https://{$this->_host}.instagram.com" . $formData['action'];

        return $formData;
    }

    /**
     * @param $formData
     * @throws CheckpointException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function checkpointThirdStep($formData)
    {
        $request = new \GuzzleHttp\Psr7\Request('POST', $formData['action']);

        $response = $this->_instagram->client->api($request, [
            'headers' => [
                'referer' => $this->_checkpointUrl,
                'x-csrftoken' => $this->_token,
            ],
            'form_params' => $formData['data'],
        ]);

        if (!preg_match('/dismiss/', $response->getBody()->getContents())) {
            throw new CheckpointException("Ошибка на последнем шаге");
        }

        if ($this->_debug)
            ConsoleHelpers::log("Отправили код", 32);

        // Если номер используется больше 1-го раза, то отмечаем его успешным
        if ($this->_smsNumber > 1) {
            $this->_smsServiceModel->success();
        }
    }
}