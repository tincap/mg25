<?php

namespace app\models\checkpoint;


use app\components\SmsServices\BasicSmsServiceModel;
use app\models\Account;
use app\models\checkpoint\Exceptions\CheckpointException;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Instagram;

class CheckpointSmsModel
{
    /**
     * Проверячет, требует ли аккаунт подтверждения смс
     *
     * @param bool $debug
     * @param $accountId
     * @param $phoneId
     * @param $phoneNumber
     * @param $smsService
     * @param $smsNumber
     * @throws CheckpointException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \app\components\SmsServices\Exceptions\SmsServiceException
     */
    public static function fix($debug = false, $accountId, $phoneId = null, $phoneNumber = null, $smsService = null, $smsNumber = null)
    {
        $account = Account::findIdentity($accountId);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        $phone = null;

        if ($phoneId != null && $phoneNumber != null && $smsService != null) {

            $phone = [
                'id' => $phoneId,
                'number' => $phoneNumber,
            ];

            $smsServiceModel = new BasicSmsServiceModel(false, $phone, $smsNumber, $smsService);
        }

        try {

//            $account->deleteCookiesFile();

            $instagram->login($account->username, $account->password);
        } catch (InstagramException $e) {

            $discover = new Discover($instagram);
            $discover->identityCheckpointTypeByResponse($e->getResponse()->asStdClass());

            if ($discover->isSms()) {

                $checkpointSms = new CheckpointSms($instagram, $discover->getHTML(), $discover->getCheckpointUrl(), $discover->getCheckpointType(), $debug, $phone, $smsNumber, $smsService);
                $checkpointSms->doFix(CheckpointSms::HOST_I);

                try {
                    $instagram->login($account->username, $account->password);

                    sleep(1);

                    $selfInfo = $instagram->people->getSelfInfo();

                    sleep(1);

                    $instagram->account->setNameAndPhone(
                        $selfInfo->getUser()->getFullName(),
                        ''
                    );

                    $account->status = Account::STATUS_ACTIVE;
                    $account->checkpoint_type = null;
                    $account->save();
                    return;


                } catch (InstagramException $e) {
                    $discover = new Discover($instagram);
                    $account->checkpoint_type = $discover->identityCheckpointTypeByResponse($e->getResponse()->asStdClass());
                    $account->status = Account::STATUS_BROKEN;
                    $account->save();

                    if (isset($smsServiceModel)) {
                        $smsServiceModel->cancel();
                    }

                    throw new CheckpointException("Не сумели отредактировать аккаунт после починки");
                }
            } else {

                if (isset($smsServiceModel)) {
                    $smsServiceModel->cancel();
                }

                $account->checkpoint_type = Account::CHECKPOINT_TYPE_UNKNOWN;
                $account->save();

                throw new CheckpointException("Аккаунт не требует подтверждения смс");
            }
        }
    }
}