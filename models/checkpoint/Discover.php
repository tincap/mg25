<?php

namespace app\models\checkpoint;
use app\models\Account;
use app\models\helpers\ConsoleHelpers;
use InstagramAPI\Instagram;


/**
 * Класс для определения вида checkpoint
 */
class Discover
{
    CONST FOLLOW_STATUS_FEEDBACK = 'feedback';
    CONST FOLLOW_STATUS_LOGIN = 'logic';

    /** @var Instagram */
    protected $instagram;

    /** @var string */
    protected $html;

    /** @var string */
    protected $checkpointUrl;

    /** @var string */
    protected $checkpointType;

    /**
     * Discover constructor.
     * @param $instagram
     */
    public function __construct($instagram)
    {
        $this->instagram = $instagram;
    }

    /**
     * @return string
     */
    public function getCheckpointType()
    {
        return $this->checkpointType;
    }

    /**
     * @return string
     */
    public function getHTML()
    {
        return $this->html;
    }

    /**
     * @return string
     */
    public function getCheckpointUrl()
    {
        return $this->checkpointUrl;
    }

    /**
     * @return bool
     */
    public function isSms()
    {
        if ($this->checkpointType == Account::CHECKPOINT_TYPE_SMS || $this->checkpointType == Account::CHECKPOINT_TYPE_SMS_GO_BACK || $this->checkpointType == Account::CHECKPOINT_TYPE_SMS_ADD_PHONE) {
            return true;
        }

        return false;
    }

    public function isBanned()
    {
        if ($this->checkpointType == Account::CHECKPOINT_TYPE_INCORRECT_USERNAME || $this->checkpointType == Account::CHECKPOINT_TYPE_DISABLED) {
            return true;
        }

        return false;
    }

    /**
     * По Response определяет вид ошибки у аккаунта
     *
     * @param $response mixed
     * @return null|string
     */
    public function identityCheckpointTypeByResponse($response)
    {
        if (isset($response->error_type)) {

            // Неверный пароль
            if (preg_match("/The password you entered is incorrect|bad_password/i", $response->error_type)) {
                $this->checkpointType = Account::CHECKPOINT_TYPE_INCORRECT_PASSWORD;
                return $this->checkpointType;
            }

            // Сброшен пароль
            if (preg_match("/unusable_password/i", $response->error_type)) {
                $this->checkpointType = Account::CHECKPOINT_TYPE_PASSWORD;
                return $this->checkpointType;
            }
        }

        // Забанен или неверное имя пользователя
        if (isset($response->message)) {

            if ($response->message instanceof \stdClass) {
                return null;
            }

            if (preg_match('/The username you entered doesn/iu', $response->message)) {
                $this->checkpointType = Account::CHECKPOINT_TYPE_INCORRECT_USERNAME;
                return $this->checkpointType;
            }

            if (preg_match('/Your account has been disabled/iu', $response->message)) {
                $this->checkpointType = Account::CHECKPOINT_TYPE_DISABLED;
                return $this->checkpointType;
            }

            if (preg_match('/login_required/', $response->message)) {
                $this->checkpointType = Account::CHECKPOINT_TYPE_LOGIN_REQUIRED;
                return $this->checkpointType;
            }
        }

        if (isset($response->challenge)) {
            if (is_array($response->challenge)) {
                $this->checkpointUrl = $response->challenge['url'];
            } else {
                $this->checkpointUrl = $response->challenge->url;
            }
        } elseif (isset($response->checkpoint_url)) {
            $this->checkpointUrl = $response->checkpoint_url;
        }

        $request = new \GuzzleHttp\Psr7\Request('GET', $this->checkpointUrl);

        $response = $this->instagram->client->api($request);

        $this->html = $response->getBody()->getContents();

        return $this->identityCheckpointTypeByHtml($this->html);
    }

    /**
     * По HTML определяет вид ошибки у аккаунта
     *
     * @param $html string
     * @return string
     */
    public function identityCheckpointTypeByHtml($html)
    {
        if (preg_match('/(enter your phone number)/iu', $html)) {
            $this->checkpointType = Account::CHECKPOINT_TYPE_SMS;
            return $this->checkpointType;
        } elseif (preg_match('/phone number ending/iu', $html)) {
            // Надо нажать кнопку "Go back"
            $this->checkpointType = Account::CHECKPOINT_TYPE_SMS_GO_BACK;
            return $this->checkpointType;

        } elseif (preg_match('/(SubmitPhoneNumberForm)|(VerifySMSCodeForm)/iu', $html)) {
            $this->checkpointType = Account::CHECKPOINT_TYPE_SMS_ADD_PHONE;
            return $this->checkpointType;

        } elseif (preg_match('/(send you a security code to verify your account)|(Enter Security Code)|(SelectVerificationMethodForm)|(contact_point)|(SelectContactPointRecoveryForm)|(ReviewLoginForm)/iu', $html)) {
            $this->checkpointType = Account::CHECKPOINT_TYPE_EMAIL;
            return $this->checkpointType;
        } elseif (preg_match('/Please enter the text below to verify your account and continue using Instagram/i', $html)) {
            $this->checkpointType = Account::CHECKPOINT_TYPE_CAPTCHA;
            return $this->checkpointType;
        }

        return null;
    }
}