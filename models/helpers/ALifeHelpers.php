<?php

namespace app\models\helpers;


class ALifeHelpers
{
    public static function getRandomStar()
    {
        $starsContent = file_get_contents(\Yii::getAlias('@app/data/files/stars.txt'));

        $persons = explode("\n", $starsContent);

        $persons = preg_replace("/\s*/", '', $persons);

        $str = $persons[array_rand($persons)];

        $items = explode(':', $str);

        $starUid = $items[0];
        $starUsername = $items[1];

        return [
            'uid' => $starUid,
            'username' => $starUsername,
        ];
    }
}