<?php

namespace app\models\helpers;


class FileHelpers
{
    /**
     * Возвращает с файла первую строку, удаляя при этом ее
     *
     * @param $filename
     * @param int $lengthStr
     * @return string
     */
    public static function getFirstStrByFile($filename, $lengthStr = 999)
    {
        $firstNameListFilename = \Yii::getAlias($filename);
        $firstNameListFile = fopen($firstNameListFilename, 'r');

        $element = preg_replace("/\n/", "", fgets($firstNameListFile, $lengthStr));

        $element = preg_replace("/\r/", "", $element);

        // Удаляем первую строку
        $file = file($firstNameListFilename);
        unset($file[0]);

        $firstNameListFile = fopen($firstNameListFilename, 'w');
        fputs($firstNameListFile, implode("", $file));

        fclose($firstNameListFile);

        return $element;
    }

    public static function getElementByFile($border, $filename) {
        $fileContent = file_get_contents($filename);
        $elementsArray = explode($border, $fileContent);

        $element = array_pop($elementsArray);

        $fp = fopen($filename, 'w');
        fputs($fp, implode($border, $elementsArray));

        fclose($fp);

        return (string) $element;
    }

    public static function addStr($filename, $str) {
        $fp = fopen($filename, 'a+');
        fwrite($fp, $str . "\n");
        fclose($fp);
    }

    public static function deleteDirectory($delfile)
    {
        if (file_exists($delfile))
        {
//            chmod($delfile,0777);
            if (is_dir($delfile))
            {
                $handle = opendir($delfile);
                while($filename = readdir($handle))

                    if ($filename != "." && $filename != "..")
                    {
                        self::deleteDirectory($delfile."/".$filename);
                    }

                closedir($handle);
                rmdir($delfile);
            }
            else
            {
                unlink($delfile);
            }
        }
    }

    /**
     * Кол-во файлов
     *
     * @param $path
     * @return int
     */
    public static function countFile($path)
    {
        $dir = opendir($path);
        $count = 0;
        while($file = readdir($dir)) {

            if($file == '.' || $file == '..' || is_dir($path . $file)){
                continue;
            }

            $count++;
        }

        return $count;
    }

    /**
     * Кол-во папрок
     *
     * @param $dirs
     * @return int
     */
    public static function countDir($dirs)
    {
        $i = 0;
        $list = scandir($dirs);
        foreach($list as $dr)
        {
            if ($dr!='.' AND $dr!='..')
            {
                if (is_dir($dirs."/".$dr)) $i++;
            }
        }
        return $i;
    }
}