<?php

namespace app\models\helpers;


class InstagramHelpers
{
    public static function getRandomWebUserAgent() {
        $userAgentContent = file_get_contents(\Yii::getAlias('@app/data/files/user-agent-list.txt'));
        $userAgentList = explode("\n", $userAgentContent);

        return preg_replace("/\s*/", "", $userAgentList[array_rand($userAgentList)]);
    }
}