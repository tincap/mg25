<?php

namespace app\models\helpers;


use app\components\simple_html_dom;
use app\models\StableProxy;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use InstagramAPI\Signatures;

class ParserHelper
{
    /**
     * Возвращает информацию и данные form на странице
     *
     * @param $html
     * @param int $formNumber
     * @param array $beforehandData - заранее заданные параметры
     * @return array|boolean
     */
    public static function getFormData($html, $formNumber = 0, $beforehandData = [])
    {
        $dom = new simple_html_dom();
        $dom->load($html);

        $form = $dom->find('form', $formNumber);

        if (!isset($form)) {
            return null;
        }

        $action = htmlspecialchars_decode($form->action);

        $inputs = $form->find('input');

        $data = [];

        foreach ($inputs as $input) {
            $data[$input->name] = html_entity_decode($input->value);
        }

        $select = $form->find('select');

        foreach ($select as $selectItem) {
            $data[$selectItem->name] = null;
        }

        $buttons = $form->find('button');

        foreach ($buttons as $button) {
            $data[$button->name] = html_entity_decode($button->value);
        }

        foreach ($beforehandData as $key => $value) {
            $data[$key] = $value;
        }

        return [
            'action' => $action,
            'data' => $data,
        ];
    }

    /**
     * @param $username
     * @return array
     * @throws GuzzleException
     * @throws \Exception
     */
    public function getImagesInfo($username)
    {
        $proxy = $this->getRandomProxy();

        $request = new Request('GET',"https://www.instagram.com/$username/");

        $client = new Client();

        $response = $client->send($request, [
            'headers' => [
                'User-Agent' => InstagramHelpers::getRandomWebUserAgent(),
                'Accept-Language' => 'ru,en-us;q=0.7,en;q=0.3',
                'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8',
            ],
            'proxy' => StringHelpers::getProxyGuzzleFormat($proxy->ip, $proxy->password),
        ]);

        $page = $this->parserUserPage($username);

        $photos = $this->getUserPhotoHrefs($page);

        $photosInfo = array();

        // Пробегаем по всем фотографиям аккаунта
        foreach ($photos as $photo) {

            $request = new Request('GET',$photo['href']);

            $response = $client->send($request, [
                'headers' => [
                    'User-Agent' => InstagramHelpers::getRandomWebUserAgent(),
                    'Accept-Language' => 'ru,en-us;q=0.7,en;q=0.3',
                    'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8',
                ],
                'proxy' => StringHelpers::getProxyGuzzleFormat($proxy->ip, $proxy->password),
            ]);

            $photoPageInfoJSON = $this->getJSON($response->getBody()->getContents());

            $comments = $this->getPhotoComments($photoPageInfoJSON);

            if (count($comments) == 0 || (count($comments) != 0 && $comments[count($comments) - 1]['author'] == $username)) {
                continue;
            }

            $photosInfo[] = array(
                'id' => $photo['id'],
                'href' => $photo['href'],
                'image' => $this->getPhoto($page),
                'comments' => $comments,
            );
        }

        return $photosInfo;
    }

    /**
     * @param $postLink
     * @return bool
     * @throws GuzzleException
     */
    public static function getMediaId($postLink)
    {
        $proxy = self::getRandomProxy();

        $request = new Request('GET',$postLink);

        $client = new Client();

        $response = $client->send($request, [
            'headers' => [
                'User-Agent' => InstagramHelpers::getRandomWebUserAgent(),
                'Accept-Language' => 'ru,en-us;q=0.7,en;q=0.3',
                'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8',
            ],
            'proxy' => StringHelpers::getProxyGuzzleFormat($proxy->ip, $proxy->password),
        ]);

        preg_match('/content="instagram:\/\/media\?id=(\d+)"/', $response->getBody()->getContents(), $found);

        if (isset($found[1])) {
            return $found[1];
        }

        return false;
    }

    /**
     * @param $page
     * @param null $link
     * @return array
     *
     * Возвращает массив ссылок типа https://instagram.com/p/2JmkWkdmwPd
     * @throws \Exception
     */
    public static function getUserPhotoHrefs($page, $link = null) {
        $photos = [];

        $json = self::getJSON($page);

        if (!$json) {
            throw new \Exception('Белая странца: ' . $link);
        }

        if (isset($json->entry_data->ProfilePage)) {
            $nodes = $json->entry_data->ProfilePage[0]->graphql->user->edge_owner_to_timeline_media->edges;
        } else if (isset($json->entry_data->UserProfile)) {
            $nodes = $json->entry_data->UserProfile[0]->userMedia;
        } else {
            throw new \Exception('Не разобрались в JSON: ' . $link);
        }

        if (isset($nodes)) {
            foreach ($nodes as $node) {
                $node = $node->node;
                $photos[] = [
                    'id' => $node->id,
                    'href' => 'https://www.instagram.com/p/' . $node->shortcode . '/',
                ];
            }
        }

        return $photos;
    }

    /**
     * @param $username
     * @return mixed
     *
     * Парсит страницу аккаунта и возвращает page
     * @throws GuzzleException
     */
    public static function parserUserPage($username)
    {
        $proxy = self::getRandomProxy();

        $request = new Request('GET',"https://www.instagram.com/{$username}/");

        $client = new Client();

        $response = $client->send($request, [
            'headers' => [
                'User-Agent' => InstagramHelpers::getRandomWebUserAgent(),
                'Accept-Language' => 'ru,en-us;q=0.7,en;q=0.3',
                'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8',
            ],
            'proxy' => StringHelpers::getProxyGuzzleFormat($proxy->ip, $proxy->password),
        ]);

        return $response->getBody()->getContents();
    }

    /**
     * @param $page
     * @return mixed
     *
     * Получает page и возвращает данные в виде JSON
     */
    public static function getJSON($page)
    {
        preg_match_all("|window._sharedData = (.*);<\/script>|", $page, $sharedData);

        if (isset($sharedData[1][0])) {
            return json_decode($sharedData[1][0]);
        }

        return array();
    }

    public function getImageCommentsCount($json, $idImage)
    {
        $count = 0;

        if (isset($json->entry_data->ProfilePage)) {
            $count = $json->entry_data->ProfilePage[0]->user->media->nodes[$idImage]->comments->count;
        }
        else if ($json->entry_data->UserProfile) {
            $count = $json->entry_data->UserProfile[0]->userMedia[$idImage]->comments->count;
        }

        return $count;
    }

    /**
     * @param $json
     * @return array
     *
     * Получает JSON данные и возвращает массив комментарий.
     *
     * comment[x][author] логин автора комментария
     * comment[x][text] текст комментария
     */
    public function getPhotoComments($json)
    {
        $comments = [];

        if (isset($json->entry_data->PostPage)) {
            $nodes = $json->entry_data->PostPage[0]->graphql->shortcode_media->edge_media_to_comment->edges;
        } else if (isset($json->entry_data->DesktopPPage)) {
            $nodes = $json->entry_data->DesktopPPage[0]->graphql->shortcode_media->edge_media_to_comment->edges;
        }

        if (isset($nodes)) {
            foreach ($nodes as $node) {
                $comments[] = array(
                    'id' => $node->node->id,
                    'author' => $node->node->owner->username,
                    'text' => $node->node->text
                );
            }
        }

        return $comments;
    }

    /**
     * @param $page
     * @return mixed
     *
     * Возвращает изображение со страницы просмотра фотографии. Вид image.jpg
     */
    public function getPhoto($page)
    {
        preg_match_all("|<meta property=\"og:image\" content=\"(.*)\" \/>|", $page, $image);

        return $image[1][0];
    }

    public static function getRandomProxy()
    {
        return StableProxy::findIdentity(rand(1, StableProxy::find()->count('*')));
    }
}