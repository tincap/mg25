<?php

namespace app\models\helpers;


class StringHelpers
{
    public static function getRandomText($length = null) {

        if ($length == null) {
            $length = rand(1, 8);
        }

        $text = "It goes without saying, books are our teachers and friends They teach us to be kind, clever, polite, hardworking, friendly Books help us to learn more about nature, the world around us and many other interesting things There are a lot of books on history, about animals, travellers, children, school and so on Children like to read adventure and magic books, science fiction and detective stories They enjoy stories, short stories, novels, fairy-tales, fables and poems We must keep books clean and tidy We must not spoil them I am fond of reading, too I like to read fairytales My favourite book is «Mary Poppins» The author of the book is Pamela Travers She is a famous English writer The book tells us about wonderful adventures of children with their magic nurse Mary Poppins She is my favourite character She is kind and strict at the same time, joyful and tactful, well-bred and very pretty Mary Poppins is popular with childrenOur Earth is so beautiful. There, are a lot of blue rivers and lakes on the Earth. Its oceans are full of wonders. There are high mountains covered with snow and wonderful fields and forests full of different plants and animals. The sun shines high up in the blue sky. At night we can see the moon and millions of stars. One season comes after another and brings changes in weather and nature. There are so many wonderful places to visit and interesting things to see. Nature gives people its riches to live and enjoy.We can’t live without fresh air, clean water, sunshine and a lot of things which we take from the nature. That’s why we must take care of it. We must keep our rivers and lakes, forests and towns clean. We must take care of each plant and each animal. We must plant flowers — not pick them up, feed birds and animals in winter — not kill them. Then we’ll be happy to live on the most beautiful planet in the Universe.I usually have four meals a day. In the morning I have breakfast. At school I have lunch. At home I have dinner and in the evening I have supper. Besides, I like to eat an apple or a banana, or to drink a glass of juice between meals, if I’m thirsty.Yesterday I got up at 7 o’clock, washed, cleaned teeth and dressed. Then I had breakfast. I had mashed potatoes with meat, a cheese sandwich and a cup of tea for breakfast. At school we had a pie and a glass of orange juice. I’m always hungry when I come home after school. Yesterday my mother, cooked cabbage soup, roast chicken and rice, made pudding and tea. It was so tasty. I ate everything with pleasure.For supper we had pancakes with jam and sour cream. These are my favourite things.My mother thinks that an apple a day keeps the doctor away. That’s why she buys fruit and vegetables every day. Yesterday she bought oranges and kiwi. I have a sweet tooth and my parents often buy me ice-cream, sweets, chocolate, cakes, cookies and other tasty things. I like them very much.Many people are fond of pets. They keep different animals and birds as pets. More often they are dogs, cats, hamsters, guinea-pigs, parrots and fish.As for me I like parrots. They are my favourite pets. They are clever and nice. I’ve got a parrot. His name is Kesha. He’s blue. He’s not big, he’s little. He has got a small head, a yellow beak, a short neck, two beautiful wings and a long tail. He lives in a cage.I teach him to talk. He knows many words and can speak well. He can answer to his name. I take care of my pet. I give him food and water every day. He likes fruit and vegetables. He likes to fly, play and talk.I love him very much. He is a member of our family.";

        $e = explode(" ", $text);

        $text = '';

        for ($i = 0; $i <= $length; $i++) {
            $text .= $e[array_rand($e)] . ' ';
        }

        return $text;
    }

    public static function getProxyGuzzleFormat($proxyIp, $proxyPassword)
    {
        return "http://{$proxyPassword}@{$proxyIp}";
    }

    /**
     * Возвращает случайное имя пользователя
     *
     * @return string
     */
    public static function getRandomUsername()
    {
        $firstnameFile = file_get_contents(\Yii::getAlias('@app/data/files/firstnames.txt'));
        $surnameFile = file_get_contents(\Yii::getAlias('@app/data/files/surnames.txt'));


        $firstnameStrList = explode("\n", $firstnameFile);
        $surnameStrList = explode("\n", $surnameFile);

        $firstname = $firstnameStrList[array_rand($firstnameStrList)];
        $surname = $surnameStrList[array_rand($surnameStrList)];

        $firstname = preg_replace("/\s*/", "", $firstname);
        $surname = preg_replace("/\s*/", "", $surname);

        $delimiter = '_';
//        if (rand(1, 2) == 1) {
//            $delimiter = '.';
//        } else {
//            $delimiter = '_';
//        }

        $username = $firstname . $delimiter . $surname;
//        if (rand(1, 2) == 1) {
//            $username = $firstname . $delimiter . $surname;
//        } else {
//            $username = $surname . $delimiter . $firstname;
//        }


        $username = $username . $delimiter . rand(90, 99);

        return $username;
    }
}