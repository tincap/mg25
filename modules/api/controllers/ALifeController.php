<?php

namespace app\modules\api\controllers;


use app\models\Account;
use app\models\checkpoint\Discover;
use app\models\helpers\ALifeHelpers;
use app\models\helpers\StringHelpers;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Instagram;

class ALifeController extends Controller
{
    const RESULT_STATUS_OK = 1;
    const RESULT_STATUS_ACCOUNT_ERROR = 2; // Ошибка с аккаунтом
    const RESULT_STATUS_FEEDBACK = 3; // Feedback
    const RESULT_STATUS_LOGIN_REQUIRED = 4; // login_required
    const RESULT_STATUS_MANY_REQUEST = 5; // Many request
    const RESULT_STATUS_UNKNOWN = 6; // Unknown

    public function actionHashtagSearch($accountId)
    {
        $account = Account::findIdentity($accountId);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        try {
            $instagram->login($account->username, $account->password);

            $instagram->hashtag->search(StringHelpers::getRandomText(1));

            return [
                'status' => 'ok',
                'result_status' => self::RESULT_STATUS_OK,
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        }
    }

    public function actionCommentTimeline($accountId)
    {
        $account = Account::findIdentity($accountId);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        try {
            $instagram->login($account->username, $account->password);

            $timelineFeed = $instagram->timeline->getTimelineFeed();

            $items = $timelineFeed->getFeedItems();

            $randomItem = $items[array_rand($items)];

            sleep(rand(1, 2));
            $instagram->media->comment($randomItem->getMediaOrAd()->getId(), StringHelpers::getRandomText());

            return [
                'status' => 'ok',
                'result_status' => self::RESULT_STATUS_OK,
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        }
    }

    public function actionLikeTimeline($accountId)
    {
        $account = Account::findIdentity($accountId);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        try {
            $instagram->login($account->username, $account->password);

            $timelineFeed = $instagram->timeline->getTimelineFeed();

            $items = $timelineFeed->getFeedItems();

            $randomItem = $items[array_rand($items)];

            sleep(rand(1, 2));
            $instagram->media->like($randomItem->getMediaOrAd()->getId());

            return [
                'status' => 'ok',
                'result_status' => self::RESULT_STATUS_OK,
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        }
    }

    /**
     * Лайк рандомного поста звезды
     *
     * @param $accountId
     * @return array
     */
    public function actionLikeRandomStarPost($accountId)
    {
        $account = Account::findIdentity($accountId);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        if ($account->status != Account::STATUS_ACTIVE) {

            $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
            $account->save();

            return [
                'status' => 'fail',
                'result_status' => self::RESULT_STATUS_ACCOUNT_ERROR,
                'message' => 'Аккаунт не работает',
            ];
        }

        $star = ALifeHelpers::getRandomStar();

        try {
            $instagram->login($account->username, $account->password);

            $response = $instagram->timeline->getUserFeed($star['uid']);

            $items = $response->getItems();

            $item = $items[array_rand($items)];

            $mediaId = $item->getId();

            sleep(rand(3, 5));

            $instagram->media->like($mediaId);

            $account->follow_status = Account::FOLLOW_STATUS_ACTIVE;
            $account->save();

            return [
                'status' => 'ok',
                'star_username' => $star['username'],
                'result_status' => self::RESULT_STATUS_OK,
            ];

        } catch (InstagramException $e) {

            // Feedback
            if (preg_match('/Feedback required|feedback_required/i', $e->getMessage())) {

                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_FEEDBACK,
                    'star_username' => $star['username'],
                    'message' => $e->getMessage(),
                ];
            }

            if (preg_match('/login_required/i', $e->getMessage())) {

                $account->checkpoint_type = Account::CHECKPOINT_TYPE_LOGIN_REQUIRED;
                $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
                $account->status = Account::STATUS_BROKEN;
                $account->save();

                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_LOGIN_REQUIRED,
                    'star_username' => $star['username'],
                    'message' => $e->getMessage(),
                ];
            }

            // СЛИШКОМ МНОГО ЗАПРОСОВ
            if (preg_match('/Throttled by Instagram because of too many API requests/i', $e->getMessage()) || preg_match('/Please wait a few minutes before you try again/i', $e->getMessage())) {

                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_MANY_REQUEST,
                    'star_username' => $star['username'],
                    'message' => $e->getMessage(),
                ];
            }

            $discover = new Discover($instagram);

            if ($e->getResponse() == null) {
                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_UNKNOWN,
                    'star_username' => $star['username'],
                    'message' => $e->getMessage(),
                ];
            }

            $checkpointType = $discover->identityCheckpointTypeByResponse($e->getResponse()->asStdClass());

            $account->checkpoint_type = $checkpointType;
            $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
            $account->status = Account::STATUS_BROKEN;
            $account->save();

            return [
                'status' => 'fail',
                'result_status' => self::RESULT_STATUS_ACCOUNT_ERROR,
                'star_username' => $star['username'],
                'message' => $account->checkpoint_type,
            ];
        }
    }

    /**
     * Комментарий рандомного поста здезды
     *
     * @param $accountId
     * @return array
     */
    public function actionCommentRandomStarPost($accountId)
    {
        $account = Account::findIdentity($accountId);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        if ($account->status != Account::STATUS_ACTIVE) {

            $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
            $account->save();

            return [
                'status' => 'fail',
                'result_status' => self::RESULT_STATUS_ACCOUNT_ERROR,
                'message' => 'Аккаунт не работает',
            ];
        }

        $star = ALifeHelpers::getRandomStar();

        try {
            $instagram->login($account->username, $account->password);

            $response = $instagram->timeline->getUserFeed($star['uid']);

            $items = $response->getItems();

            $item = $items[array_rand($items)];

            $mediaId = $item->getId();

            sleep(rand(3, 5));

            $instagram->media->comment($mediaId, StringHelpers::getRandomText());

            return [
                'status' => 'ok',
                'star_username' => $star['username'],
                'result_status' => self::RESULT_STATUS_OK,
            ];

        } catch (InstagramException $e) {

            // Feedback
            if (preg_match('/Feedback required|feedback_required/i', $e->getMessage())) {

                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_FEEDBACK,
                    'star_username' => $star['username'],
                    'message' => $e->getMessage(),
                ];
            }

            if (preg_match('/login_required/i', $e->getMessage())) {

                $account->checkpoint_type = Account::CHECKPOINT_TYPE_LOGIN_REQUIRED;
                $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
                $account->status = Account::STATUS_BROKEN;
                $account->save();

                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_LOGIN_REQUIRED,
                    'star_username' => $star['username'],
                    'message' => $e->getMessage(),
                ];
            }

            // СЛИШКОМ МНОГО ЗАПРОСОВ
            if (preg_match('/Throttled by Instagram because of too many API requests/i', $e->getMessage()) || preg_match('/Please wait a few minutes before you try again/i', $e->getMessage())) {

                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_MANY_REQUEST,
                    'star_username' => $star['username'],
                    'message' => $e->getMessage(),
                ];
            }

            $discover = new Discover($instagram);

            if ($e->getResponse() == null) {
                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_UNKNOWN,
                    'star_username' => $star['username'],
                    'message' => $e->getMessage(),
                ];
            }

            $checkpointType = $discover->identityCheckpointTypeByResponse($e->getResponse()->asStdClass());

            $account->checkpoint_type = $checkpointType;
            $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
            $account->status = Account::STATUS_BROKEN;
            $account->save();

            return [
                'status' => 'fail',
                'result_status' => self::RESULT_STATUS_ACCOUNT_ERROR,
                'star_username' => $star['username'],
                'message' => $account->checkpoint_type,
            ];
        }
    }

    /**
     * Авторизация
     *
     * @param $accountId
     * @return array
     */
    public function actionUpdateLogin($accountId)
    {
        $account = Account::findIdentity($accountId);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        try {
            $instagram->login($account->username, $account->password);

            return [
                'status' => 'ok',
                'result_status' => self::RESULT_STATUS_OK,
            ];
        } catch (InstagramException $e) {
            $discover = new Discover($instagram);

            if ($e->getResponse() == null) {
                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_UNKNOWN,
                    'message' => $e->getMessage(),
                ];
            }

            $checkpointType = $discover->identityCheckpointTypeByResponse($e->getResponse()->asStdClass());

            $account->checkpoint_type = $checkpointType;
            $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
            $account->status = Account::STATUS_BROKEN;
            $account->save();

            return [
                'status' => 'fail',
                'result_status' => self::RESULT_STATUS_ACCOUNT_ERROR,
                'message' => $account->checkpoint_type,
            ];
        }
    }

    /**
     * Комментарий рандомного поста
     *
     * @param $accountId
     * @return array
     */
    public function actionReadRandomStar($accountId)
    {
        $account = Account::findIdentity($accountId);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        if ($account->status != Account::STATUS_ACTIVE) {

            $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
            $account->save();

            return [
                'status' => 'fail',
                'result_status' => self::RESULT_STATUS_ACCOUNT_ERROR,
                'star_username' => '',
                'message' => 'Аккаунт не работает',
            ];
        }

        $star = ALifeHelpers::getRandomStar();

        try {
            $instagram->login($account->username, $account->password);

            sleep(1);

            $instagram->timeline->getUserFeed($star['uid']);

            return [
                'status' => 'ok',
                'star_username' => $star['username'],
                'result_status' => self::RESULT_STATUS_OK,
            ];

        } catch (InstagramException $e) {

            // Feedback
            if (preg_match('/Feedback required|feedback_required/i', $e->getMessage())) {

                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_FEEDBACK,
                    'star_username' => $star['username'],
                    'message' => $e->getMessage(),
                ];
            }

            if (preg_match('/login_required/i', $e->getMessage())) {

                $account->checkpoint_type = Account::CHECKPOINT_TYPE_LOGIN_REQUIRED;
                $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
                $account->status = Account::STATUS_BROKEN;
                $account->save();

                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_LOGIN_REQUIRED,
                    'star_username' => $star['username'],
                    'message' => $e->getMessage(),
                ];
            }

            // СЛИШКОМ МНОГО ЗАПРОСОВ
            if (preg_match('/Throttled by Instagram because of too many API requests/i', $e->getMessage()) || preg_match('/Please wait a few minutes before you try again/i', $e->getMessage())) {

                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_MANY_REQUEST,
                    'star_username' => $star['username'],
                    'message' => $e->getMessage(),
                ];
            }

            $discover = new Discover($instagram);

            if ($e->getResponse() == null) {
                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_UNKNOWN,
                    'star_username' => $star['username'],
                    'message' => $e->getMessage(),
                ];
            }

            $checkpointType = $discover->identityCheckpointTypeByResponse($e->getResponse()->asStdClass());

            $account->checkpoint_type = $checkpointType;
            $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
            $account->status = Account::STATUS_BROKEN;
            $account->save();

            return [
                'status' => 'fail',
                'result_status' => self::RESULT_STATUS_ACCOUNT_ERROR,
                'star_username' => $star['username'],
                'message' => $account->checkpoint_type,
            ];
        }
    }
}