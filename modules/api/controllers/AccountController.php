<?php

namespace app\modules\api\controllers;


use app\models\Account;
use app\models\checkpoint\Discover;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Instagram;

class AccountController extends Controller
{
    const RESULT_STATUS_OK = 1;
    const RESULT_STATUS_ACCOUNT_ERROR = 2; // Ошибка с аккаунтом
    const RESULT_STATUS_FEEDBACK = 3; // Feedback
    const RESULT_STATUS_LOGIN_REQUIRED = 4; // login_required
    const RESULT_STATUS_MANY_REQUEST = 5; // Many request
    const RESULT_STATUS_UNKNOWN = 6; // Unknown

    /**
     * Подписаться
     *
     * @param $accountId
     * @param $uid
     * @return array
     */
    public function actionFollow($accountId, $uid)
    {
        $account = Account::findIdentity($accountId);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        if ($account->status != Account::STATUS_ACTIVE) {

            $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
            $account->save();

            return [
                'status' => 'fail',
                'result_status' => self::RESULT_STATUS_ACCOUNT_ERROR,
                'message' => 'Аккаунт не работает',
            ];
        }

        if ($account->zaliv == null) {

            $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
            $account->save();

            return [
                'status' => 'fail',
                'result_status' => self::RESULT_STATUS_ACCOUNT_ERROR,
                'message' => 'У аккаунта нет заливки',
            ];
        }

        try {
            $instagram->login($account->username, $account->password);

            sleep(1);

            $instagram->people->follow($uid);

            $account->follow_status = Account::FOLLOW_STATUS_ACTIVE;
            $account->save();

            return [
                'status' => 'ok',
                'result_status' => self::RESULT_STATUS_OK,
            ];

        } catch (InstagramException $e) {

            // Feedback
            if (preg_match('/Feedback required|feedback_required/i', $e->getMessage())) {

                $account->follow_status = Account::FOLLOW_STATUS_FEEDBACK;
                $account->save();

                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_FEEDBACK,
                    'message' => $e->getMessage(),
                ];
            }

            if (preg_match('/login_required/i', $e->getMessage())) {

                $account->checkpoint_type = Account::CHECKPOINT_TYPE_LOGIN_REQUIRED;
                $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
                $account->status = Account::STATUS_BROKEN;
                $account->save();

                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_LOGIN_REQUIRED,
                    'message' => $e->getMessage(),
                ];
            }

            // СЛИШКОМ МНОГО ЗАПРОСОВ
            if (preg_match('/Throttled by Instagram because of too many API requests/i', $e->getMessage()) || preg_match('/Please wait a few minutes before you try again/i', $e->getMessage())) {

                $account->follow_status = self::RESULT_STATUS_MANY_REQUEST;
                $account->save();

                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_MANY_REQUEST,
                    'message' => $e->getMessage(),
                ];
            }

            $discover = new Discover($instagram);

            if ($e->getResponse() == null) {
                return [
                    'status' => 'fail',
                    'result_status' => self::RESULT_STATUS_UNKNOWN,
                    'message' => $e->getMessage(),
                ];
            }

            $checkpointType = $discover->identityCheckpointTypeByResponse($e->getResponse()->asStdClass());

            $account->checkpoint_type = $checkpointType;
            $account->follow_status = Account::FOLLOW_STATUS_NO_ACTIVE;
            $account->status = Account::STATUS_BROKEN;
            $account->save();

            return [
                'status' => 'fail',
                'result_status' => self::RESULT_STATUS_ACCOUNT_ERROR,
                'message' => $account->checkpoint_type,
            ];
        }
    }

    /**
     * Авторизация
     *
     * @param $accountId
     * @return array
     */
    public function actionLogin($accountId)
    {
        $account = Account::findIdentity($accountId);

        Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        try {
            $instagram->login($account->username, $account->password);

            $account->status = Account::STATUS_ACTIVE;
            $account->checkpoint_type = null;
            $account->save();

            return [
                'status' => 'ok',
            ];
        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        }
    }

    /**
     * Количество аккаунтов
     *
     * @return array
     */
    public function actionCount()
    {
        return [
            'status' => 'ok',
            'count' => Account::find()->count('id'),
        ];
    }

    /**
     * Проверка статуса Friendship
     *
     * @param $accountId
     * @param $uid
     * @return array
     */
    public function actionCheckFriendshipStatus($accountId, $uid)
    {
        $account = Account::findIdentity($accountId);

        if ($account->status != Account::STATUS_ACTIVE) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        try {
            $instagram->login($account->username, $account->password);

            sleep(1);

            $response = $instagram->people->getFriendship($uid);

            if ($response->getOutgoingRequest() || $response->getFollowing()) {
                return [
                    'status' => 'ok',
                    'follow' => true
                ];
            }


            return [
                'status' => 'ok',
                'follow' => false,
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        }
    }

    /**
     * a-life
     *
     * @param $accountId
     * @return array
     */
    public function actionALife($accountId)
    {
        $account = Account::findIdentity($accountId);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        try {
            $instagram->login($account->username, $account->password);

            sleep(rand(3, 7));

            $response = $instagram->people->getSelfFollowers();

            sleep(rand(3, 7));

            $users = $response->getUsers();

            $randomFollower = $users[array_rand($users)];

            $result = $instagram->timeline->getUserFeed($randomFollower->getPk());

            $items = $result->getItems();

            if (rand(1, 10) == 10) {

                sleep(rand(3, 7));

                $randomMedia = $items[array_rand($items)];

                $instagram->media->like($randomMedia->getId());
            }

            return [
                'status' => 'ok',
            ];
        } catch (InstagramException $e) {
            return [
                'status' => 'fail',
                'message' => 'Не смог авторизоваться. ' . $e->getMessage(),
            ];
        }
    }

    /**
     * Возвращает файл со списком подписок аккаунта
     *
     * @param $accountId
     * @return array
     */
    public function actionGetAccountFollowListFile($accountId)
    {
        $account = Account::findIdentity($accountId);

        return [
            'status' => 'ok',
            'filename' => $account->getFollowListFile(),
        ];
    }

    /**
     * Сохранить кол-во подписок
     *
     * @param $accountId
     * @param $followCount
     * @return array
     */
    public function actionUpdateFollowCount($accountId, $followCount)
    {
        $account = Account::findIdentity($accountId);
        $account->follow_count = $followCount;
        $account->save();

        return [
            'status' => 'ok',
        ];
    }

    public function actionIsCaption($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->caption) {
            return [
                'status' => 'ok',
                'caption' => true,
            ];
        }

        return [
            'status' => 'ok',
            'caption' => false,
        ];
    }

    public function actionIsPhoto($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->photo) {
            return [
                'status' => 'ok',
                'photo' => true,
            ];
        }

        return [
            'status' => 'ok',
            'photo' => false,
        ];
    }

    public function actionIsBiographed($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->biographed) {
            return [
                'status' => 'ok',
                'biographed' => true,
            ];
        }

        return [
            'status' => 'ok',
            'biographed' => false,
        ];
    }

    public function actionIsClear($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->clear) {
            return [
                'status' => 'ok',
                'clear' => true,
            ];
        }

        return [
            'status' => 'ok',
            'clear' => false,
        ];
    }

    public function actionSetClear($accountId)
    {
        $account = Account::findIdentity($accountId);
        $account->clear = 1;
        $account->save();

        return [
            'status' => 'ok',
        ];
    }

    public function actionSetBroken($accountId)
    {
        $account = Account::findIdentity($accountId);
        $account->status = Account::STATUS_BROKEN;
        $account->save();

        return [
            'status' => 'ok',
        ];
    }

    public function actionFollowStatusLogic($accountId)
    {
        $account = Account::findIdentity($accountId);
        $account->follow_status = Account::FOLLOW_STATUS_LOGIC;
        $account->save();

        return [
            'status' => 'ok',
        ];
    }

    public function actionIsAvatar($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->avatar) {
            return [
                'status' => 'ok',
                'avatar' => true,
            ];
        }

        return [
            'status' => 'ok',
            'avatar' => false,
        ];
    }

    public function actionIsActive($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->status == Account::STATUS_ACTIVE) {
            return [
                'status' => 'ok',
                'active' => true,
            ];
        }

        return [
            'status' => 'ok',
            'active' => false,
        ];
    }
}