<?php

namespace app\modules\api\controllers;


use app\components\SmsServices\BasicSmsServiceModel;
use app\components\SmsServices\Exceptions\SmsServiceException;
use app\models\Account;
use app\models\checkpoint\CheckpointSmsModel;
use app\models\checkpoint\Exceptions\CheckpointException;
use GuzzleHttp\Exception\GuzzleException;

class CheckpointSmsController extends Controller
{
    /**
     * Чинит аккаунт
     *
     * @param $accountId
     * @param $phoneId
     * @param $phoneNumber
     * @param $smsService
     * @param int $smsNumber
     * @return array
     */
    public function actionFix($accountId, $phoneId, $phoneNumber, $smsService, $smsNumber = 1)
    {
        if ($phoneId == null || $phoneNumber == null || $smsService == null) {
            return [
                'status' => 'ok',
                'message' => 'Empty params',
            ];
        }

        try {

            CheckpointSmsModel::fix(false, $accountId, $phoneId, $phoneNumber, $smsService, $smsNumber);
            $account = Account::findIdentity($accountId);
            $account->status = Account::STATUS_ACTIVE;
            $account->checkpoint_type = null;
            $account->save();

            return [
                'status' => 'ok',
            ];
        } catch (SmsServiceException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        } catch (CheckpointException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        } catch (\Exception $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage() . ' in ' . $e->getFile() . ' ' .  $e->getLine(),
            ];
        } catch (GuzzleException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Получить доступный номер
     *
     * @param $smsService
     * @param int $country
     * @return array
     */
    public function actionGetNumber($smsService, $country = 0)
    {
        $smsServiceModel = new BasicSmsServiceModel(false, null, 1, $smsService, $country);

        try {
            $phone = $smsServiceModel->getNumber();

            if (is_array($phone)) {

                return [
                    'status' => 'ok',
                    'phone_id' => (string) $phone['id'],
                    'phone_number' => (string) $phone['number'],
                    'sms_service' => $smsServiceModel->getSmsService(),
                ];

            } else {
                throw new SmsServiceException("Не смогли получить номер");
            }

        } catch (\Exception $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        } catch (GuzzleException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Возвращает список id аккаунтов, которые требует подтверждения
     *
     * @return array
     */
    public function actionGetBrokenAccountList()
    {
        $idList = Account::find()->select('id')->where('checkpoint_type = "' . Account::CHECKPOINT_TYPE_SMS_ADD_PHONE . '" OR checkpoint_type = "' . Account::CHECKPOINT_TYPE_SMS . '" OR checkpoint_type = "' . Account::CHECKPOINT_TYPE_SMS_GO_BACK .'"')->asArray()->all();

        $brokenAccountList = [];

        foreach ($idList as $item) {
            $brokenAccountList[] = (int) $item['id'];
        }

        return [
            'status' => 'ok',
            'broken_account_list' => $brokenAccountList,
        ];
    }
}