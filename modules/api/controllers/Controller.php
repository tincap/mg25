<?php

namespace app\modules\api\controllers;

use app\models\Account;
use app\models\checkpoint\Discover;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Instagram;

class Controller extends \yii\web\Controller
{
    public $enableCsrfValidation = false;

    /**
     * Обработка ошибок типа InstagramException
     *
     * @param InstagramException $e
     * @param Instagram $instagram
     * @param Account $account
     * @return array
     */
    public function catchErrors(InstagramException $e, Instagram $instagram, Account $account)
    {
        if ($e->getResponse() == null) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }

        $discover = new Discover($instagram);
        $account->checkpoint_type = $discover->identityCheckpointTypeByResponse($e->getResponse()->asStdClass());
        $account->status = Account::STATUS_BROKEN;
        $account->save();

        return [
            'status' => 'fail',
            'message' => $account->checkpoint_type == null ? $e->getMessage() : $account->translateErrorType(),
        ];
    }
}