<?php

namespace app\modules\api\controllers;


use app\components\ImageResize;
use app\components\ImageResizeException;
use app\models\Account;
use app\models\helpers\FileHelpers;
use app\models\helpers\PhotoHelpers;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Instagram;

class MediaController extends Controller
{
    /**
     * Заполнение фотографий описаниями
     *
     * @param $accountId
     * @return array
     */
    public function actionCaption($accountId)
    {
        $account = Account::findIdentity($accountId);
        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        if ($account->status != Account::STATUS_ACTIVE) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        if ($account->caption) {
            return [
                'status' => 'ok',
            ];
        }

        if (!$account->photo) {
            return [
                'status' => 'fail',
                'message' => 'Не загружены фотки на аккаунт',
            ];
        }

        $instagram->login($account->username, $account->password);

        try {

            sleep(1);
            $selfUserInfoFeed = $instagram->timeline->getSelfUserFeed();

            $items = $selfUserInfoFeed->getItems();

            $mediaCount = count($items);

            if ($mediaCount > \Yii::$app->params['media_count']) {

                $account->photo = 0;
                $account->caption = 0;
                $account->save();

                return [
                    'status' => 'fail',
                    'message' => 'Количество фоток больше чем ' . \Yii::$app->params['media_count'],
                ];
            }

            if ($mediaCount < \Yii::$app->params['media_count']) {

                $account->photo = 0;
                $account->caption = 0;
                $account->save();

                return [
                    'status' => 'fail',
                    'message' => 'Количество фоток меньше чем ' . \Yii::$app->params['media_count'],
                ];
            }

            $caption = FileHelpers::getElementByFile(\Yii::$app->params['dividing_line'], \Yii::getAlias('@app/data/files/captions.txt'));

            if (empty($caption)) {
                return [
                    'status' => 'fail',
                    'message' => 'captions.txt is empty',
                ];
            }

            sleep(1);

            foreach ($selfUserInfoFeed->getItems() as $item) {
                $instagram->media->edit($item->getId(), $caption);
                sleep(rand(\Yii::$app->params['caption_pause'][0], \Yii::$app->params['caption_pause'][1]));
            }

            $account->caption = 1;
            $account->save();

            return [
                'status' => 'ok',
            ];

        } catch (InstagramException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Загрузить фотографию
     *
     * @param $accountId
     * @param $folderId
     * @return array
     */
    public function actionPhoto($accountId, $folderId)
    {
        $account = Account::findIdentity($accountId);
        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        $success_upload = 0;

        if ($account->photo == 1) {
            return [
                'status' => 'ok',
                'success_upload' => $success_upload,
            ];
        }

        if ($account->status != Account::STATUS_ACTIVE) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
                'success_upload' => $success_upload,
            ];
        }

        try {

            $instagram->login($account->username, $account->password);

            sleep(1);
            $selfUserInfoFeed = $instagram->timeline->getSelfUserFeed();

            $mediaCount = count($selfUserInfoFeed->getItems());

            if ($mediaCount == \Yii::$app->params['media_count']) {
                $account->photo = 1;
                $account->save();

                return [
                    'status' => 'ok',
                    'success_upload' => 0,
                ];
            }

            sleep(1);
            for ($i = $mediaCount + 1; $i <= \Yii::$app->params['media_count']; $i++) {

                $photoPath = \Yii::getAlias("@app/data/photos/$folderId/" . $i . '.jpg');

                if (!file_exists($photoPath)) {
                    $photoPath = \Yii::getAlias("@app/data/photos/$folderId/" . $i . '.JPG');
                }

                if (!file_exists($photoPath)) {
                    $photoPath = \Yii::getAlias("@app/data/photos/$folderId/" . $i . '.jpeg');
                }

                if (!file_exists($photoPath)) {
                    $photoPath = \Yii::getAlias("@app/data/photos/$folderId/" . $i . '.JPEG');
                }

                if (!file_exists($photoPath)) {
                    return [
                        'status' => 'fail',
                        'message' => "Не нашли фотографию $photoPath",
                        'success_upload' => $success_upload,
                    ];
                }

                $imageResize = new ImageResize($photoPath);

                if ($imageResize->getDestWidth() > \Yii::$app->params['photo_size'] || $imageResize->getDestHeight() > \Yii::$app->params['photo_size']) {

                    if ($imageResize->getDestWidth() > $imageResize->getDestHeight()) {
                        $imageResize->resizeToWidth(\Yii::$app->params['photo_size']);
                        $imageResize->save($photoPath);
                    } else {
                        $imageResize->resizeToHeight(\Yii::$app->params['photo_size']);
                        $imageResize->save($photoPath);
                    }
                }

                if ($imageResize->getDestWidth() > $imageResize->getDestHeight()) {
                    PhotoHelpers::horizontal($photoPath);
                } else {
                    if ($imageResize->getDestWidth() < $imageResize->getDestHeight()) {
                        PhotoHelpers::vertical($photoPath);
                    }
                }

                $imageResize = null;

                $instagram->timeline->uploadPhoto($photoPath);
                $success_upload++;

                sleep(rand(\Yii::$app->params['photo_pause'][0], \Yii::$app->params['photo_pause'][1]));
            }

        } catch (InstagramException $e) {
            return [
                'status' => 'fail',
                'message' => "Не смогли загрузить фотографию: " . $e->getMessage(),
                'success_upload' => $success_upload,
            ];
        } catch (ImageResizeException $e) {
            return [
                'status' => 'fail',
                'message' => "Не смогли загрузить фотографию: " . $e->getMessage(),
                'success_upload' => $success_upload,
            ];
        }

        $account->photo = 1;
        $account->save();

        return [
            'status' => 'ok',
            'success_upload' => $success_upload,
        ];
    }

    /**
     * Вернуть кол-во папок с фотографиями
     *
     * @return array
     */
    public function actionCountPhotosFolder()
    {
        $directory = \Yii::getAlias("@app/data/photos");

        if (!is_dir($directory)) {
            return [
                'status' => 'fail',
                'message'=> 'Не нашли папку upload',
            ];
        }

        $count = FileHelpers::countDir($directory);

        return [
            'status' => 'ok',
            'count' => $count,
        ];
    }

    /**
     * Удалить media аккаунта
     *
     * @param $accountId
     * @param $mediaId
     * @return array
     */
    public function actionDelete($accountId, $mediaId)
    {
        $account = Account::findIdentity($accountId);
        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        if ($account->status != Account::STATUS_ACTIVE) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        try {
            $instagram->login($account->username, $account->password);

            sleep(1);

            $instagram->media->delete($mediaId);

            return [
                'status' => 'ok',
            ];
        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        }
    }

    /**
     * Возвращает список media аккаунта
     *
     * @param $accountId
     * @return array
     */
    public function actionGetList($accountId)
    {
        $account = Account::findIdentity($accountId);
        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        if ($account->status != Account::STATUS_ACTIVE) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        try {
            $instagram->login($account->username, $account->password);

            sleep(1);

            $response = $instagram->timeline->getSelfUserFeed();

            $mediaList = [];

            foreach ($response->getItems() as $item) {
                $mediaList[] = $item->getId();
            }

            return [
                'status' => 'ok',
                'mediaIdList' => $mediaList,
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        }
    }
}