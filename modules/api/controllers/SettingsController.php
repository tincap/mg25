<?php

namespace app\modules\api\controllers;


use app\models\Account;
use app\models\helpers\FileHelpers;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Instagram;

class SettingsController extends Controller
{
    /**
     * Загружает аватарку
     *
     * @param $accountId
     * @return array
     */
    public function actionAvatar($accountId)
    {
        $account = Account::findIdentity($accountId);
        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        if ($account->status != Account::STATUS_ACTIVE) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        if ($account->avatar == 1) {
            return [
                'status' => 'ok',
            ];
        }

        try {

            $instagram->login($account->username, $account->password);

            sleep(1);

            $countDir = FileHelpers::countDir(\Yii::getAlias("@app/data/photos"));

            if ($countDir < 5) {
                return [
                    'status' => 'fail',
                    'message' => 'Слишком мало папок в @app/data/photos',
                ];
            }

            $folderId = rand(1, FileHelpers::countDir(\Yii::getAlias("@app/data/photos")));
            $photoId = rand(1, 3);

            $photoPath = \Yii::getAlias("@app/data/photos/{$folderId}/{$photoId}.jpg");

            if (!file_exists($photoPath)) {
                $photoPath = \Yii::getAlias("@app/data/photos/$folderId/" . $photoId . '.JPG');
            }

            if (!file_exists($photoPath)) {
                $photoPath = \Yii::getAlias("@app/data/photos/$folderId/" . $photoId . '.jpeg');
            }

            if (!file_exists($photoPath)) {
                $photoPath = \Yii::getAlias("@app/data/photos/$folderId/" . $photoId . '.JPEG');
            }

            if (!file_exists($photoPath)) {
                return [
                    'status' => 'fail',
                    'message' => 'Не нашли файл ' . $photoPath,
                ];
            }

            $instagram->account->changeProfilePicture($photoPath);

            $account->avatar = 1;
            $account->save();

            return [
                'status' => 'ok'
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        }
    }

    /**
     * Изменить имя
     *
     * @param $accountId
     * @return array
     */
    public function actionFirstname($accountId)
    {
        $account = Account::findIdentity($accountId);
        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        if ($account->status != Account::STATUS_ACTIVE) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        if ($account->firstname) {
            return [
                'status' => 'ok',
            ];
        }

        try {

            $instagram->login($account->username, $account->password);

            sleep(1);

            $firstnamePath = \Yii::getAlias('@app/data/files/firstnames.txt');
            $firstname = FileHelpers::getFirstStrByFile($firstnamePath);

            $info = $instagram->people->getSelfInfo();

            sleep(1);

            if (empty($firstname)) {
                return [
                    'status' => 'fail',
                    'message' => 'firstnameList.txt пустой',
                ];
            }

            $instagram->account->editProfile(
                '',
                '',
                $firstname,
                $info->getUser()->getBiography(),
                $account->email->username,
                2
            );

            $account->firstname = 1;
            $account->save();

            return [
                'status' => 'ok',
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        } catch (\Exception $e) {
            return [
                'status' => 'fail',
                'message' => "Не смогли загрузить описание: " . $e->getMessage(),
            ];
        }
    }

    /**
     * Описание профиля
     *
     * @param $accountId
     * @return array
     */
    public function actionBiography($accountId)
    {
        $account = Account::findIdentity($accountId);

        Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        if ($account->status != Account::STATUS_ACTIVE) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        if ($account->biographed) {
            return [
                'status' => 'ok',
            ];
        }

        try {

            $instagram->login($account->username, $account->password);

            sleep(1);

            $biographyPath = \Yii::getAlias('@app/data/files/biography.txt');
            $biography = FileHelpers::getElementByFile(\Yii::$app->params['dividing_line'], $biographyPath);

            if (empty($biography)) {
                return [
                    'status' => 'fail',
                    'message' => 'biography.txt is empty',
                ];
            }

            $currentUser = $instagram->account->getCurrentUser();

            sleep(1);

            $instagram->account->editProfile('', '', $currentUser->getUser()->getFullName(), $biography, $account->email->username, $currentUser->getUser()->getGender());

            $account->biographed = 1;
            $account->save();

            return [
                'status' => 'ok',
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        }
    }

    /**
     * @param $accountId
     * @param $newZalivName
     * @return array
     * @throws \Exception
     */
    public function actionReplace($accountId, $newZalivName)
    {
        if (!preg_match('/@/', $newZalivName)) {
            $newZalivName = '@' . $newZalivName;
        }

        $account = Account::findIdentity($accountId);
        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        if ($newZalivName == $account->zaliv) {
            return [
                'status' => 'ok',
            ];
        }

        if ($account->status != Account::STATUS_ACTIVE) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        try {

            $instagram->login($account->username, $account->password);

            sleep(1);

            $response = $instagram->timeline->getSelfUserFeed();

            foreach ($response->getItems() as $item) {

                $newCaption = preg_replace('/@\w+/', $newZalivName, $item->getCaption()->getText());

                $instagram->media->edit($item->getId(), $newCaption);

                sleep(rand(\Yii::$app->params['replace_pause'][0], \Yii::$app->params['replace_pause'][1]));
            }

            $account->zaliv = $newZalivName;
            $account->save();

            return [
                'status' => 'ok',
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        }
    }

    /**
     * Замена заливок
     *
     * @param $accountId
     * @param $oldZalivName
     * @param $newZalivName
     * @return array
     */
    public function actionReplaceByZaliv($accountId, $oldZalivName, $newZalivName)
    {
        if (!preg_match('/@/', $oldZalivName)) {
            $oldZalivName = '@' . $oldZalivName;
        }

        if (!preg_match('/@/', $newZalivName)) {
            $newZalivName = '@' . $newZalivName;
        }

        $account = Account::findIdentity($accountId);
        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        if ($newZalivName == $account->zaliv) {
            return [
                'status' => 'ok',
            ];
        }

        if ($account->zaliv != $oldZalivName) {
            return [
                'status' => 'ok',
            ];
        }

        if ($account->status != Account::STATUS_ACTIVE) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        try {

            $instagram->login($account->username, $account->password);

            sleep(1);

            $response = $instagram->timeline->getSelfUserFeed();

            foreach ($response->getItems() as $item) {

                $newCaption = preg_replace('/@\w+/', $newZalivName, $item->getCaption()->getText());

                $instagram->media->edit($item->id, $newCaption);

                sleep(rand(\Yii::$app->params['replace_pause'][0], \Yii::$app->params['replace_pause'][1]));
            }

            $account->zaliv = $newZalivName;
            $account->save();

            return [
                'status' => 'ok',
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        }
    }


    public function actionUpdateInfo($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->status != Account::STATUS_ACTIVE) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает'
            ];
        }

        $instagram = new Instagram();
        $instagram->setProxy($account->getProxyGuzzleFormat());

        try {

            $instagram->login($account->username, $account->password);

            $response = $instagram->people->getSelfInfo();

                $account->media_count = $response->getUser()->getMediaCount();
                $account->follower_count = $response->getUser()->getFollowerCount();
                $account->following_count = $response->getUser()->getFollowingCount();

                // Верхнее описание
                if (strlen($response->getUser()->getBiography()) < 90) {
                    $account->biographed = 0;
                }

            $response = $instagram->timeline->getSelfUserFeed();

            $zalivName = null;

            foreach ($response->getItems() as $item) {

                $account->zaliv = null;
                $account->save();

                if ($item->getCaption() == null)
                {
                    return [
                        'status' => 'fail',
                        'message' => '- без заливки'
                    ];
                }

                preg_match('/(@\w+)/iu', $item->getCaption()->getText(), $found);

                if (isset($found[1])) {
                    if ($zalivName != null) {
                        if ($zalivName != $found[1]) {
                            $account->zaliv = null;
                            $account->caption = 0;

                            return [
                                'status' => 'fail',
                                'message' => '- без заливки'
                            ];
                        }
                    } else {
                        $zalivName = $found[1];
                    }

                    $account->zaliv = $found[1];
                    $account->photo = 1;
                    $account->caption = 1;
                } else {
                    $account->zaliv = null;
                    $account->caption = 0;
                }
            }

            $account->save();

            if ($account->zaliv != null) {
                return [
                    'status' => 'ok',
                    'zaliv_name' => $account->zaliv,
                ];
            }

            return [
                'status' => 'fail',
                'message' => '- без заливки'
            ];
        } catch (InstagramException $e) {
            return $this->catchErrors($e, $instagram, $account);
        }
    }
}