<?php

namespace app\modules\api\controllers;


use app\models\Account;
use app\models\helpers\StringHelpers;
use app\models\StableProxy;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Instagram;

class ZalivController extends Controller
{
    public function actionCheckLink($username)
    {
        $proxyCount = StableProxy::find()->count();

        $proxyId = rand(1, $proxyCount);

        $proxy = StableProxy::findIdentity($proxyId);

        $request = new Request('GET',"https://www.instagram.com/$username/");

        $client = new Client();

        try {
            $response = $client->send($request, [
                'headers' => [
                    'Accept-Language' => 'ru-ru',
                ],
                'proxy' => StringHelpers::getProxyGuzzleFormat($proxy->ip, $proxy->password),
            ]);
        } catch (GuzzleException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }

        $html = $response->getBody()->getContents();

        if (preg_match('/страница недоступна/iu', $html)) {
            return [
                'status' => 'ok',
                'id' => Account::findByUsername($username)->id,
                'username' => $username,
                'working' => 0,
            ];
        }

        preg_match('/"external_url":"([^"]*)"/iu', $html, $found);

        $url = "";

        if (isset($found[1])) {
            $url = $found[1];
        }

        return [
            'status' => 'ok',
            'id' => Account::findByUsername($username)->id,
            'username' => $username,
            'working' => 1,
            'url' => $url,
        ];
    }

    public function actionDirectCheck($accountId)
    {
        $account = Account::findIdentity($accountId);

        $instagram = new Instagram();

        $instagram->setProxy($account->getProxyGuzzleFormat());

        try {
            $instagram->login($account->username, $account->password);

            $inbox = $instagram->direct->getInbox();

            $unreadThreadList = $account->getUnreadThreadList($inbox, $instagram);

            /** @var \InstagramAPI\Response\DirectPendingInboxResponse $pendingThreads */
            $pendingInbox = $instagram->direct->getPendingInbox();

            $messageList = array_merge($unreadThreadList, $pendingInbox->getInbox()->getThreads());

            return [
                'status' => 'ok',
                'count_new_message' => count($messageList),
            ];

        } catch (InstagramException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }
    }
}