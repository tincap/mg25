<?php

/* @var $this yii\web\View */

use app\models\Account;

/* @var $accountList \app\models\Account[] */

$this->title = 'Список аккаунтов';

?>

<table class="table table-bordered table-striped table-hover">

    <?php

    $smsCount = 0;

    // Кол-во аккаунтов которые фолловят
    $followAccountCount = 0;

    foreach ($accountList as $account) {
        if ($account->isSms()) {
            $smsCount++;
        }
    }

    ?>

    <thead class="thead-inverse">
    <tr>
        <th><?= count($accountList) ?></th>
        <th>Список аккаунтов</th>
        <th>Заполнение</th>
        <th>Заливка</th>
        <th>Follow</th>
        <th class="text-danger" width="100px"><?= $smsCount ?></th>
    </tr>
    </thead>
    <tbody>

    <?php
    foreach ($accountList as $account) :
        ?>

        <tr>
            <td width="40px" class="account_id"><?= $account->id ?></td>
            <td><a class="username" target="_blank" href="https://instagram.com/<?= $account->username ?>"><?= $account->username . ':' . $account->password ?></a></td>
            <td>
                <?= $account->photo ? ' <span style="color:#2aba32" title="Заполнение">P</span>' : ' <span style="color:#ba0001" title="Заполнение">P</span>' ?>
                <?= $account->biographed ? ' <span style="color:#2aba32" title="Описание">B</span>' : ' <span style="color:#ba0001" title="Описание">B</span>' ?>
            </td>
            <td>
                <?= $account->zaliv != null ? '<a class="username" target="_blank" href="https://instagram.com/' . str_replace('@', '', $account->zaliv) . '">' . $account->zaliv . '</a> | ' : '' ?>
                <span class="text-success"><?= $account->follower_count !== null ? $account->follower_count . ' | ' : '' ?></span>
                <span class="text-danger"><?= $account->following_count !== null ? $account->following_count . ' | ' : '' ?></span>
                <strong><?= $account->media_count ?></strong>
            </td>

            <td>

                <?php

                echo $account->follow_count;

                switch ($account->follow_status) {

                    case Account::FOLLOW_STATUS_ACTIVE :

                        echo ' | <strong style="color:#2f9432">ON</strong>';
                        break;

                    case Account::FOLLOW_STATUS_FEEDBACK :

                        echo ' | <strong style="color:#151fba">FEEDBACK</strong>';
                        break;

                    case Account::FOLLOW_STATUS_LOGIC :

                        echo ' | <strong style="color:#f6a142">LOGIC</strong>';
                        break;

                    case Account::FOLLOW_STATUS_NO_ACTIVE :

                        echo ' | <strong style="color:#8f8f94">OFF</strong>';
                        break;


                }

                ?>

            </td>

            <td width="40px" class="<?= $account->status == \app\models\Account::STATUS_ACTIVE ? 'success' : ($account->checkpoint_type == \app\models\Account::CHECKPOINT_TYPE_DISABLED ? 'danger' : 'warning') ?>"><?= $account->status == \app\models\Account::STATUS_ACTIVE ? '' : $account->translateErrorType() ?></td>
        </tr>

    <?php
    endforeach;
    ?>

    </tbody>
</table>

