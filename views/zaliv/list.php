<?php

/* @var $accountList Account[] */

use yii\web\View;
use \app\models\Account;

/* @var $this yii\web\View */

$this->title = 'Список аккаунтов';

$this->registerJsFile('@web/js/zaliv/main-list.js', ['position' => View::POS_END]);
$this->registerJsFile('@web/js/zaliv/direct-check.js', ['position' => View::POS_END]);
?>

<div class="site-index">

    <table class="table table-bordered table-striped table-hover">
        <thead class="thead-inverse">
        <tr>
            <th><?= count($accountList) ?></th>
            <th>Список аккаунтов</th>
            <th>Ссылка</th>
            <th>Кол-во рассылок</th>
            <th>Директ</th>
            <th class="text-danger" width="100px"></th>
        </tr>
        </thead>
        <tbody>

        <?php
        foreach ($accountList as $account) :
            ?>

            <tr>
                <td width="40px" class="account_id"><?= $account->id ?></td>
                <td><a class="username" target="_blank" href="https://instagram.com/<?= $account->username ?>"><?= $account->username ?></a></td>
                <td id="account_<?= $account->id ?>">

                </td>
                <td>
                    <?= $account->message_count ?>
                </td>
                <td width="200px" class="direct" id="direct_<?= $account->id ?>" data-id="<?= $account->id ?>">
                    <a class="message_list" target="_blank" href="/zaliv/thread-list/<?= $account->id ?>">
                    </a>
                </td>
                <td width="40px" class="<?= $account->status == Account::STATUS_ACTIVE ? 'success' : 'warning' ?>">
                    <?= $account->status == Account::STATUS_ACTIVE ? '' : $account->translateErrorType() ?>
                </td>
            </tr>

        <?php
        endforeach;
        ?>

        </tbody>
    </table>

</div>