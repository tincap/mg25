$(document).ready(function() {

    $('.comment').each(function () {

        var id = $(this).attr('data-id');
        var count_new_comment;

        $.ajax({
            type: "GET",
            url: "/api/zaliv/comment-check/" + id,
            dataType: 'json',
            success: function (response) {
                if (response.status === 'ok') {

                    count_new_comment = response.count_new_comment;

                    if (count_new_comment > 0) {
                        $('#comment_' + id).addClass('active');
                        $('#comment_' + id + ' a').append('+' + count_new_comment);
                    }
                }

                if (response.status === 'fail') {
                    $('#comment_' + id).addClass('danger');
                    $('#comment_' + id).append('Ошибка');
                }
            }
        });
    });
});