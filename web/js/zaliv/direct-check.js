$(document).ready(function() {

    $('.direct').each(function () {

        var id = $(this).attr('data-id');
        var count_new_messages;

        $.ajax({
            type: "GET",
            url: "/api/zaliv/direct-check/" + id,
            dataType: 'json',
            success: function (response) {
                if (response.status === 'ok') {

                    /** @namespace response.count_new_message */
                    count_new_messages = response.count_new_message;

                    if (count_new_messages > 0) {
                        $('#direct_' + id).addClass('active');
                        $('#direct_' + id + ' a').append('Новых сообщений: ' + count_new_messages);
                    } else {
                        $('#direct_' + id).append("Нет сообщений");
                    }
                }

                if (response.status === 'fail') {
                    $('#direct_' + id).addClass('danger');
                    $('#direct_' + id).append('Аккаунт не работает');
                }
            }
        });
    });
});