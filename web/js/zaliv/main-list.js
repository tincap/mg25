$(document).ready(function() {
    var username;

    $('.username').each(function () {

        username = $(this).text();

        $.ajax({
            type: "GET",
            url: "/api/zaliv/check-link/" + username,
            dataType: 'json',
            success: function (response) {
                if (response.working) {
                    $('#account_' + response.id).addClass('success');
                    $('#account_' + response.id).append(response.url);
                } else {
                    $('#account_' + response.id).addClass('danger');
                }
            }
        });
    });
});